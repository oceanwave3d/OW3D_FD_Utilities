function [D, nvec]=GaussianIntegration2D_WLS_SetUp(x,u,StencilMap,EtoV,D)
%
% Given a vector of scattered coordinate positions x(1:N,3) and the 
% corresponding (u,v)-space u(1:N,1:2), use order-p WLS operators to
% interpolate the surface gradients to compute the normal vector and the 
% elements of surface area at each grid point. Then, again using order-p WLS 
% interpolation, form the Gaussian integration vector that integrates a 
% function over all of the triangles defined by the element to vertex map 
% EtoV(1:nel,3). Input parameters that need to be in the structure D
% are: 
%       D(1,1).p         The order of the WLS schemes.
%       D(1,1).np        The number of stencil points in the WLS scheme.
%       D(1,1).Wtype     The WLS weight function: 'Gaussian', otherwise uniform.
%       D(1,1).sigma     The non-dimensional exponent for the Gaussian. 
%       D(1,1).Wmin      The relative cut-off value for how small a weight can become. 
%
% The function returns: 
%
%       D(m,n).D         The sparse derivative matrix for d^m/du^m d^n/dv^n.
%       nvec(1:N,3)      The unit normal vector to each grid point x.
%       D(1,1).Ivec(1,N) The integration vector for this grid. 
%
% The integral of the function f(1:N,1) is obtained from: 
%
%       I = D(1,1).Ivec*f;
%
% The Gaussian integration schemes that I've implemented are, 1. From C.T.
% Reddy, "Improved Three Point Integration Schemes For Triangular Finite
% Elements" Num. Methods in Eng. (1978), a 6-point scheme that is
% 3rd-order accurate. 2. A 7-point scheme from Reddy & Shippy (1981), it is
% 4th-order. 3. The Matlab function 'quadtriangle.m' by Ethan
% Kubatko which is arbitrary order. If '3' is chosen, then the quadtriangle
% package must be in the path. 
%
% More detailed discussion of the theory and implementation can be found in: 
%
% "A note on finite differences in multiple-domain boundary-fitted 
% coordinates" 
%  by
% H. B. Bingham, DTU, 2024. 
%
% Written by H.B. Bingham, DTU
%
% This version: Oct. 5, 2024.
%
%%
% Extract the parameters of the WLS scheme.
%
N=size(x,1); p=D(1,1).p; np=D(1,1).np; Wtype=D(1,1).Wtype; 
sigma=D(1,1).sigma; Wmin=D(1,1).Wmin;
nTaylor=(p+1)^2;
c=zeros(nTaylor,np);

%%
% Build the required 2D WLS derivative operators. Here we only need first
% derivatives. 
%
ierr=0; D(1,1).isingular=[];
for i=1:N
    errflag=0;
    xp(1:np,1:2)=u(StencilMap(i,:),1:2); xp0=[u(i,1),u(i,2)];
    d=sqrt((xp(:,1)-xp0(1)).^2+(xp(:,2)-xp0(2)).^2);
    in= d~=0; tmp=sort(d(in)); dist=mean(tmp(1:p+1)); %dist=min(d(in)); 
    s=sigma*dist;
    switch Wtype
        case 'Gaussian'
            w=exp(-d.^2/s^2);
            w(w<=Wmin)=Wmin;  % Limit the minimum value of the weight
        otherwise
            w=ones(np,1);
    end
    [c(:,1:np),errflag]=FDInterpolate2D_WLS(xp0,xp,p,w,dist);
    if errflag ~= 0
        ierr=ierr+1; D(1,1).isingular(ierr)=i;
    end
    for m=1:2
        for n=1:2
            k=(m-1)*(p+1)+n;
            D(m,n).D(i,StencilMap(i,:))=c(k,1:np);
        end
    end
end
%
% Take all of the coordinate derivatives and build the normal vector and
% the element of the surface area. 
%
xu(:,1)=D(2,1).D*x(:,1); xu(:,2)=D(2,1).D*x(:,2); xu(:,3)=D(2,1).D*x(:,3);
xv(:,1)=D(1,2).D*x(:,1); xv(:,2)=D(1,2).D*x(:,2); xv(:,3)=D(1,2).D*x(:,3);
nvec=zeros(N,3); E=zeros(N,1); F=E; G=E;
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    nvec(i,1)=-(xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2));
    nvec(i,2)=-(xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3));
    nvec(i,3)=-(xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1));
end
H=sqrt(E.*G-F.^2); % The Jacobian which is also just the element of surface area.
for i=1:3
    nvec(:,i)=nvec(:,i)./H; % The unit normal vector 
end

nel=size(EtoV,1); % The number of triangles on the body.
%%
% Set up the Gauss integration scheme. Scheme '1' here comes from Reddy
% (1978) and is 3rd-order, scheme '2' is from Reddy & Shippy (1981) and is
% 4th-order. Scheme '3' uses the Matlab function 'quadtriangle.m' by Ethan
% Kubatko and is arbitrary order. If '3' is chosen, then the quadtriangle
% package must be in the path. 
%
Gmethod='3';
switch Gmethod
    case '1'
        nG=6;
        xiG=[0.231933383,0.1090390162;      % The Gauss point coordinates
            0.1090390162,0.6590276008;      % on the standard triangle.
            0.6590276008,0.2319333830];
        xiG=[xiG;fliplr(xiG)];
        wG=1/6*ones(nG,1);                  % The Gauss weights
    case '2'
        nG=7;
        xiG(1,:)=[1/3,1/3]; wG(1,1)=.1000526268;
        xi=.0554630355; eta=.0554632748;
        xiG(2:4,:)=[xi,eta;eta,1-xi-eta;1-xi-eta,xi]; wG(2:4,1)=0.0209871755;
        xi=.2956689642; eta=.6337734837;
        xiG(5:7,:)=[xi,eta;eta,1-xi-eta;1-xi-eta,xi]; wG(5:7,1)=0.1123286156;
        wG=2*wG;
    case '3'
        Q=quadtriangle(p,'Type','nonproduct');
        nG=length(Q.Points); xiG=(Q.Points+[1,1])/2; 
        wG=Q.Weights/2;
end

%%
% Build the Gaussian integration vector for this grid of triangles.
%
% Initialize the integration vector and loop over elements and Gauss
% points.
%
D(1,1).Ivec=zeros(1,N); 
%
for ie=1:nel
    i=EtoV(ie,1); % The expansion point for the WLS interpolation
    % The vertices of this triangle
    u1=u(EtoV(ie,1),:); u2=u(EtoV(ie,2),:); u3=u(EtoV(ie,3),:); 
    % The Jacobian for mapping this triangle to the standard triangle.
    Jg=abs((u2(1)-u1(1))*(u3(2)-u1(2)) - (u3(1)-u1(1))*(u2(2)-u1(2)));
    xp(1:np,1:2)=u(StencilMap(i,:),:); % The stencil points
    
    Ivec=zeros(1,np); ierr=0;
    for iG=1:nG
        %
        % The Gauss points in the (u,v)-space
        %
        xp0(1)=u1(1)+xiG(iG,1)*(u2(1)-u1(1))+xiG(iG,2)*(u3(1)-u1(1));
        xp0(2)=u1(2)+xiG(iG,1)*(u2(2)-u1(2))+xiG(iG,2)*(u3(2)-u1(2));
        %
        % Set up the WLS weights.
        %
        d=sqrt((xp(:,1)-xp0(1)).^2+(xp(:,2)-xp0(2)).^2);
        in= d~=0; tmp=sort(d(in)); dist=mean(tmp(1:p+1)); 
        s=sigma*dist;
        switch Wtype
            case 'Gaussian'
                w=exp(-d.^2/s^2);
                w(w<=Wmin)=Wmin;  % Limit the minimum value of the weight
            otherwise
                w=ones(np,1);
        end
        %
        % Get the coefficients
        %
        [c(:,1:np),errflag]=FDInterpolate2D_WLS(xp0,xp,p,w,dist);
        if errflag ~= 0
            ierr=ierr+1; D(1,1).isingular(ierr)=i;
        end
        %
        % Accumulate this Gauss point contribution.
        %
        Ivec=Ivec+.5*wG(iG)*c(1,:);
    end
    %
    % Scale with the Jacobian and add this contribution to the integration
    % vector. 
    %
    D(1,1).Ivec(1,StencilMap(i,:))=D(1,1).Ivec(1,StencilMap(i,:))+Jg*Ivec;
end
%
% Scale with the elements of area and return.
%
D(1,1).Ivec=D(1,1).Ivec .* H'; 

end
