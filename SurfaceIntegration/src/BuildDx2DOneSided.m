function Dx=BuildDx2DOneSided(Nx,Ny,x,alpha,der)
%
% A function to build the sparse der^th x-derivative operator matrix on a 
% structured but non-uniform 2D grid.   
% One-sided schemes are used towards the boundaries.  The grid 
% is assumed to follow y first from bottom left to top right.  
%
%    der must of course be <= r-1; where r=2*alpha+1.
%
% Written by H.B. Bingham, DTU, 2009
%
%%
r=2*alpha+1; N=Nx*Ny;
if der > r-1, error('This stencil does not support that derivative'); end
%
%%
% Loop over the grid points and collect the FD coefficients for each row of
% the matrix.
%
index=0;
for i=1:alpha               % One-sided points to the left.
    c=FornbergInterpolate(x(i),x(1:r),r,der);
    for j=1:Ny              % Loop over y.
        im=(i-1)*Ny+j; jp=j;
        for irel=1:r        % Stencil points for this grid point.
            ip=irel;
            jm=(ip-1)*Ny+jp;
            index=index+1;
            irn(index)=im; icn(index)=jm;
            Mat(index)=c(1+der,irel);
        end
    end
end
for i=alpha+1:Nx-alpha      % Central points in x.
    c=FornbergInterpolate(x(i),x(i-alpha:i+alpha),r,der);
    for j=1:Ny              % Loop over y.  
        im=(i-1)*Ny+j; jp=j;
        for irel=1:r        % Stencil points for this grid point.
            ip=i-alpha-1+irel;
            jm=(ip-1)*Ny+jp;
            index=index+1;
            irn(index)=im; icn(index)=jm;
            Mat(index)=c(1+der,irel);
        end
    end
end
for i=Nx-alpha+1:Nx         % One-sided points to the right.
    c=FornbergInterpolate(x(i),x(Nx-r+1:Nx),r,der);
    for j=1:Ny              % Loop over y.  
        im=(i-1)*Ny+j; jp=j;
        for irel=1:r        % Stencil points for this grid point.
            ip=Nx-r+irel;
            jm=(ip-1)*Ny+jp;
            index=index+1;
            irn(index)=im; icn(index)=jm;
            Mat(index)=c(1+der,irel);
        end
    end
end
Dx=spconvert([irn' icn' Mat']);  
end