function [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,u,StencilMap,EtoV,D)
%
% Given a vector of scattered coordinate positions x(1:N,3) and the 
% corresponding (u,v)-space u(1:N,1:2), use order-p WLS operators to
% interpolate the surface gradients, compute the normal vector and the 
% elements of surface area at each grid point. Order-p WLS Tyalor
% integration is then used to form the integration vector that integrates a 
% function over all of the triangles defined by the element to vertex map 
% EtoV(1:nel,3). 
%
% Input parameters that need to be in the structure D
% are: 
%       D(1,1).p         The order of the WLS schemes.
%       D(1,1).np        The number of stencil points in the WLS scheme.
%       D(1,1).Wtype     The WLS weight function: 'Gaussian', otherwise uniform.
%       D(1,1).sigma     The non-dimensional exponent for the Gaussian. 
%       D(1,1).Wmin      The relative cut-off value for how small a weight can become. 
%       D(1,1).CMethod   'Hypergeom', or 'Functions'. Choose which approach
% to use for computing the Taylor coefficients. 'Hypergeom' means use the
% closed-form integral in terms of hypergeometric functions, 'Function'
% means use the term-by-term integral form functions created by the script
% 'TaylorCoefficients_WLS_Symbolic.m'. 
%
% The function returns: 
%
%       D(m,n).D         The sparse derivative matrices for d^m/du^m d^n/dv^n.
%       nvec(1:N,3)      The unit normal vector to each grid point x.
%       D(1,1).Ivec(1,N) The integration vector for this grid. 
%
% The integral of the function f(1:N,1) is obtained from: 
%
%       I = D(1,1).Ivec*f;
%
% The Taylor integration scheme applied here is derived in the note: 
%
% "A note on finite differences in multiple-domain boundary-fitted 
% coordinates" 
%  by
% H. B. Bingham, DTU, 2024. 
%
% The Taylor coefficients on triangles are computed from the functions 
% Cmn(du1,du2,dv1,dv2) which have been created symbolically from the
% function 'TaylorCoefficients_WLS_Symbolic.m', where 0<=(m,n)<=4. These
% functions need to be in the path. If higher order solutions are needed,
% the function can be re-run with a larger limit. 
%
% I've also implemented the general solution in terms of the hypergeometric
% function, but this requires careful treatment of the vertices to avoid
% the singularity at argument=1. It is also less accurate for the volume 
% integral for some reason, though it gives the same result for the surface 
% integral. It is much slower though due to the hypergeom function, so this
% option is hard-coded to 'Functions' below. 
%
%
%%
% Written by H.B. Bingham, DTU
% This version: Oct. 5, 2024.
%
%%
% Both methods are implemented, but it's better to use the 'Functions'
% method, which is hard-coded here. 
%
CMethod='Functions';
% CMethod='Hypergeom';
%
% Extract the parameters of the WLS scheme.
%
N=size(x,1); p=D(1,1).p; np=D(1,1).np; Wtype=D(1,1).Wtype; 
sigma=D(1,1).sigma; Wmin=D(1,1).Wmin;
nTaylor=(p+1)^2;
c=zeros(nTaylor,np);

%%
% Build the required 2D WLS derivative operators up to order p. 
%
ierr=0; D(1,1).isingular=[];
for i=1:N
    errflag=0;
    xp(1:np,1:2)=u(StencilMap(i,:),1:2); xp0=[u(i,1),u(i,2)];
    d=sqrt((xp(:,1)-xp0(1)).^2+(xp(:,2)-xp0(2)).^2);
    in= d~=0; tmp=sort(d(in)); 
    %
    % I've played with several choices here for scaling the weights and the
    % best I've come up with is this one.
    %
    dist=mean(tmp(1:p+1)); 
    %dist=sqrt(mean(tmp(1:p+1)));
    %dist=mean(d(in))/2; %dist=min(d(in)); 
    s=sigma*dist;
    switch Wtype
        case 'Gaussian'
            w=exp(-d.^2/s^2);
            w(w<=Wmin)=Wmin;  % Limit the minimum value of the weight
        otherwise
            w=ones(np,1);
    end
    [c(:,1:np),errflag]=FDInterpolate2D_WLS(xp0,xp,p,w,dist);
    if errflag ~= 0
        ierr=ierr+1; D(1,1).isingular(ierr)=i;
    end
    for m=1:p+1
        for n=1:p+1
            k=(m-1)*(p+1)+n;
            D(m,n).D(i,StencilMap(i,:))=c(k,1:np);
        end
    end
end
%
% Take all of the coordinate derivatives and build the normal vector and
% the element of the surface area. 
%
xu(:,1)=D(2,1).D*x(:,1); xu(:,2)=D(2,1).D*x(:,2); xu(:,3)=D(2,1).D*x(:,3);
xv(:,1)=D(1,2).D*x(:,1); xv(:,2)=D(1,2).D*x(:,2); xv(:,3)=D(1,2).D*x(:,3);
nvec=zeros(N,3); E=zeros(N,1); F=E; G=E;
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    nvec(i,1)=-(xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2));
    nvec(i,2)=-(xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3));
    nvec(i,3)=-(xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1));
end
H=sqrt(E.*G-F.^2); % The Jacobian which is also just the element of surface area.
for i=1:3
    nvec(:,i)=nvec(:,i)./H; % The unit normal vector 
end
%
nel=size(EtoV,1); % The number of triangles on the body.
%
u1=u(EtoV(:,1),1); u2=u(EtoV(:,2),1); u3=u(EtoV(:,3),1);
v1=u(EtoV(:,1),2); v2=u(EtoV(:,2),2); v3=u(EtoV(:,3),2);
du1=u2-u1; du2=u3-u1; 
dv1=v2-v1; dv2=v3-v1; 
%
switch CMethod
    case 'Hypergeom'
        %
        % Check for du1=du2 and move the expansion point to ensure that the
        % argument to the hypergeom function is never 1. Then, re-compute the
        % distances, form the triangle areas and the expansion point map.
        %
        ie=find(abs(du2-du1)<10^-4);
        EtoV(ie,[1,2,3])=EtoV(ie,[3,1,2]);
        u1=u(EtoV(:,1),1); u2=u(EtoV(:,2),1); u3=u(EtoV(:,3),1);
        v1=u(EtoV(:,1),2); v2=u(EtoV(:,2),2); v3=u(EtoV(:,3),2);
        du1=u2-u1; du2=u3-u1;
        dv1=v2-v1; dv2=v3-v1;
        %
        % Also make sure that dv1 and dv2 are non-zero to some tolerance.
        % With a quick test, it seems that 10^-9 * the max. triangle
        % dimension works well. 
        %
        tol=10^-9*max(max(sqrt(du1.^2+dv1.^2)),max(sqrt(du2.^2+dv2.^2)));
        dv1(abs(dv1)<tol)=tol;
        dv2(abs(dv2)<tol)=tol;
end

Area=du1.*dv2-du2.*dv1; 

kj=EtoV(:,1); % The index of the expansion vertex for each triangle

switch CMethod
    case 'Hypergeom'
        %
        % The hypergeometric function arguments:
        %
        arg1=du1.*(dv2-dv1)./Area;
        arg2=du2.*(dv2-dv1)./Area;
end

%%
% Build the Taylor integration vector for this grid of triangles.
%
% Initialize the integration vector and loop over elements.
%
D(1,1).Ivec=zeros(1,N); 

for m=1:p+1
    for n=1:p+1
        switch CMethod
            case 'Hypergeom'
                coefVec=((du1.^m.*dv1.^n.*hypergeom([1,m+n],m+1,arg1) ...
                    - du2.^m.*dv2.^n.*hypergeom([1,m+n],m+1,arg2))...
                    ./((m+n)*factorial(m)*factorial(n-1)))';
                D(1,1).Ivec=D(1,1).Ivec+coefVec*D(m,n).D(kj,:);
            otherwise
                fn=strcat('C',num2str(m-1),num2str(n-1));
                C(m,n).C=feval(fn,du1',du2',dv1',dv2');
                D(1,1).Ivec=D(1,1).Ivec+C(m,n).C.*Area'*D(m,n).D(kj,:);
        end
    end
end
%
% Scale with the elements of area and return.
%
D(1,1).Ivec=D(1,1).Ivec .* H'; 
end

