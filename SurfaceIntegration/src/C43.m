function out1 = C43(du1,du2,dv1,dv2)
%C43
%    OUT1 = C43(DU1,DU2,DV1,DV2)

%    This function was generated by the Symbolic Math Toolbox version 8.5.
%    19-Jun-2022 19:16:21

t2 = du1.^2;
t3 = du1.^3;
t4 = du2.^2;
t6 = du2.^3;
t8 = dv1.^2;
t9 = dv1.^3;
t10 = dv2.^2;
t11 = dv2.^3;
t5 = t2.^2;
t7 = t4.^2;
out1 = (t5.*t9)./1.0368e+4+(t5.*t11)./3.6288e+5+(t7.*t9)./3.6288e+5+(t7.*t11)./1.0368e+4+(du2.*t3.*t9)./1.8144e+4+(du1.*t6.*t9)./9.072e+4+(du2.*t3.*t11)./9.072e+4+(du1.*t6.*t11)./1.8144e+4+(dv2.*t5.*t8)./2.4192e+4+(dv1.*t5.*t10)./7.2576e+4+(dv2.*t7.*t8)./7.2576e+4+(dv1.*t7.*t10)./2.4192e+4+(t2.*t4.*t9)./3.6288e+4+(t2.*t4.*t11)./3.6288e+4+(du2.*dv2.*t3.*t8)./1.8144e+4+(du2.*dv1.*t3.*t10)./3.024e+4+(du1.*dv2.*t6.*t8)./3.024e+4+(du1.*dv1.*t6.*t10)./1.8144e+4+(dv2.*t2.*t4.*t8)./2.016e+4+(dv1.*t2.*t4.*t10)./2.016e+4;
