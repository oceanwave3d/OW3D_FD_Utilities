function out1 = C44(du1,du2,dv1,dv2)
%C44
%    OUT1 = C44(DU1,DU2,DV1,DV2)

%    This function was generated by the Symbolic Math Toolbox version 8.5.
%    19-Jun-2022 19:16:22

t2 = du1.^2;
t3 = du1.^3;
t4 = du2.^2;
t6 = du2.^3;
t8 = dv1.^2;
t9 = dv1.^3;
t10 = dv2.^2;
t12 = dv2.^3;
t5 = t2.^2;
t7 = t4.^2;
t11 = t8.^2;
t13 = t10.^2;
out1 = (t5.*t11)./5.184e+4+(t5.*t13)./3.6288e+6+(t7.*t11)./3.6288e+6+(t7.*t13)./5.184e+4+(du2.*t3.*t11)./1.0368e+5+(du1.*t6.*t11)./7.2576e+5+(du2.*t3.*t13)./7.2576e+5+(du1.*t6.*t13)./1.0368e+5+(dv2.*t5.*t9)./1.0368e+5+(dv1.*t5.*t12)./7.2576e+5+(dv2.*t7.*t9)./7.2576e+5+(dv1.*t7.*t12)./1.0368e+5+(t2.*t4.*t11)./2.4192e+5+(t2.*t4.*t13)./2.4192e+5+(t5.*t8.*t10)./2.4192e+5+(t7.*t8.*t10)./2.4192e+5+(du2.*dv2.*t3.*t9)./9.072e+4+(du1.*dv2.*t6.*t9)./2.268e+5+(du2.*dv1.*t3.*t12)./2.268e+5+(du1.*dv1.*t6.*t12)./9.072e+4+(du2.*t3.*t8.*t10)./1.2096e+5+(du1.*t6.*t8.*t10)./1.2096e+5+(dv2.*t2.*t4.*t9)./1.2096e+5+(dv1.*t2.*t4.*t12)./1.2096e+5+(t2.*t4.*t8.*t10)./1.008e+5;
