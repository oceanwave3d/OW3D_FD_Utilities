function [D, nvec, ds]=TaylorIntegration2D_Direct_SetUp(x,u,v,p)
%
% Given a vector of coordinate positions x(1:N,3) and the corresponding
% (u,v)-space u(1:nu,1) and v(1:nv,1), set up all of the required
% derivative operators for doing Taylor integration of a function over the
% surface to order p. x(1:N,3) follows v first then u. The function
% returns: 
%
%       D(m,n).D         The sparse derivative matrix for d^m/du^m d^n/dv^n.
%       nvec(1:N,3)      The unit normal vector to each grid point x.
%       ds(1:N,1)        The element of surface area at each point x. 
%
% Written by H.B. Bingham, DTU, June 2022. Updated Oct. 5, 2024
%
%%
N=size(x,1); nu=length(u); nv=length(v); % The size of the (u,v)-space
tmp=diff(u); du=repmat(tmp,[nv-1,1]); du=du(:); % The (u,v) grid spacing.
tmp=diff(v); dv=repmat(tmp',[1,nu-1]); dv=dv(:);

alpha=p/2;  % The order of the scheme.

%%
% Build the 2D derivative operators for Taylor Integration.
%
D(1,1).D=speye(N,N);
for m=1:p
    D(m+1,1).D=BuildDx2DOneSided(nu,nv,u,alpha,m);
    D(1,m+1).D=BuildDy2DOneSided(nu,nv,v,alpha,m);
end
for m=2:p+1
    for n=2:p+1
        D(m,n).D=D(m,1).D*D(1,n).D;
    end
end
%
% Take all of the coordinate derivatives and build the normal vector and
% the element of the surface area. 
%
xu(:,1)=D(2,1).D*x(:,1); xu(:,2)=D(2,1).D*x(:,2); xu(:,3)=D(2,1).D*x(:,3);
xv(:,1)=D(1,2).D*x(:,1); xv(:,2)=D(1,2).D*x(:,2); xv(:,3)=D(1,2).D*x(:,3);
nvec=zeros(N,3);
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    nvec(i,1)=-(xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2));
    nvec(i,2)=-(xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3));
    nvec(i,3)=-(xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1));
end
ds=sqrt(E.*G-F.^2); % The Jacobian which is also just the element of surface area.
for i=1:3
    nvec(:,i)=nvec(:,i)./ds; % The unit normal vector 
end
D(1,1).nu=nu; D(1,1).nv=nv; D(1,1).du=du; D(1,1).dv=dv;
end
