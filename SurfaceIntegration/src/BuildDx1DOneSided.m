function Dx=BuildDx1DOneSided(Nx,x,alpha,der)
%
% A function to build the sparse der^th x-derivative operator matrix on a 
% structured but non-uniform 1D grid defined by the points x(1:Nx) using 
% 2*alpha+1 nearby points in each stencil. Centered schemes are used where  
% possible and off-centered schemes are used near the boundaries at both 
% ends. 
%
%    ** der <= r-1; where r=2*alpha+1 **
%
% Written by H.B. Bingham, DTU Mechanical Engineering. 
% This version: June, 2016.
%
%%
r=2*alpha+1; 
if der > r-1, error('This stencil does not support that derivative'); end
index=0;
for i=1:alpha               % One-sided points to the left.
    c=FornbergInterpolate(x(i),x(1:r),r,der); 
    for irel=1:r            % Stencil points for this grid point.
        j=irel;
        index=index+1;
        irn(index)=i; icn(index)=j;
        Mat(index)=c(der+1,irel);
    end
end
for i=alpha+1:Nx-alpha      % Central points in x.
    c=FornbergInterpolate(x(i),x(i-alpha:i+alpha),r,der);
    for irel=1:r            % Stencil points for this grid point.
        j=i-alpha-1+irel;
        index=index+1;
        irn(index)=i; icn(index)=j;
        Mat(index)=c(der+1,irel);
    end
end
for i=Nx-alpha+1:Nx         % One-sided points to the right
    c=FornbergInterpolate(x(i),x(Nx-r+1:Nx),r,der); 
    for irel=1:r            % Stencil points for this grid point.
        j=Nx-r+irel;
        index=index+1;
        irn(index)=i; icn(index)=j;
        Mat(index)=c(der+1,irel);
    end
end
Dx=spconvert([irn' icn' Mat']);  