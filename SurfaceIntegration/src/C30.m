function out1 = C30(du1,du2,dv1,dv2)
%C30
%    OUT1 = C30(DU1,DU2)

%    This function was generated by the Symbolic Math Toolbox version 8.5.
%    19-Jun-2022 19:16:20

out1 = ((du1+du2).*(du1.^2+du2.^2))./1.2e+2;
