function [Ivec,nvec]=BuildTaylorIntegrationVector1D(x,p)
%%
% This function builds the Taylor integration vector which, when multiplied
% by a vector of function values, gives the integral over the curve of 
% the function.
%
% This version should be used when the arc-lengths along the curve are not 
% known exactly in advance. The 2D curve is represented only by the 
% coordinate points x(1:n,1:2) where the function values are known.  
% p+1 is the number of Taylor series terms to include in the expansion. 
% 
% If the arc lengths are known exactly, then the function 
%
%    BuildTaylorIntegrationVector1D_ds_known()
%
% should be used instead. 
%
% More detailed discussion can be found in Sec. 4.2.3 of: 
%
% "A note on finite differences in multiple-domain boundary-fitted 
% coordinates" 
%  by
% H. B. Bingham, DTU, 2024. 
%
% The points are parametrized by the straight line distances between 
% between them, normalized to the interval from 0 to 1. 
% The integral from u=u_j to u=u_{j+1} along the arc of length du_j in 
% the parameter space is:  
%
%  I_0=sum_{n=0}^p du_j^{n+1}/(n+1)! f_j^{(n)}.  
%
% Note that the arc-length is called '\xi' in the note, but we use 'u' here.
% Adding these sub-integrals from j=1:n-1 gives the total integral. 
%
% This operation can be expressed as the dot product between the
% integration row vector Ivec(1,1:n) and the function value vector 
% g=f(1:n,1).*dsEl(1:n,1), where Ivec is found from the product 
%
%  Ivec = I(1,1:n-1) * A(1:n-1,1:n).
%
% Here I is the unit row vector and A is the matrix of Taylor expansion
% integral operators. dsEl is the chain rule factor ds/du which needs to be
% multiplied with the function being integrated to get the integral in the
% physical coordinates. 
%
%%
%
% Written by H. B. Bingham, DTU, Feb., 2024.
%
%%
n=length(x); 
%
% Build the parameter space.
%
du=sqrt((x(2:n,1)-x(1:n-1,1)).^2+(x(2:n,2)-x(1:n-1,2)).^2); 
u(1,1)=0;
for ip=2:n
    u(ip,1)=u(ip-1,1)+du(ip-1);  
end
l=(u(n,1)-u(1,1)); u=u/l; du=du/l;
%
% The 1/2 width of the FD stencil. 
%
alpha=p/2; 
%
% Build all the required parametric-space derivative matrices
%
for iorder=1:p
    Ds(iorder).D=BuildDx1DOneSided(n,u,alpha,iorder);
end
%
% Get the derivatives of the coordinates and compute the normal vector and
% the chain rule factor ds/du at each point. 
%
xu(:,1)=Ds(1).D*x(:,1); xu(:,2)=Ds(1).D*x(:,2);
nvec(:,1)=-xu(:,2);
nvec(:,2)=xu(:,1);
H(:,1)=sqrt(nvec(:,1).^2+nvec(:,2).^2);
nvec(:,1)=nvec(:,1)./H(:,1); nvec(:,2)=nvec(:,2)./H(:,1); 
%
% Build the matrix of sub-integral derivative operators
%
A=spdiags(du,0,n-1,n);
for ip=1:n-1
    for iorder=1:p
        A(ip,:)=A(ip,:)+du(ip)^(iorder+1)/factorial(iorder+1)*Ds(iorder).D(ip,:);
    end
end
%
% Multiply with the unit vector to sum all rows and form the integration
% vector, then scale each term with ds/du. 
% 
I=ones(1,n-1);
Ivec=H'.*(I*A); 

