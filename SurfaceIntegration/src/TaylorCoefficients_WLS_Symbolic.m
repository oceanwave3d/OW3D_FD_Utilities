%
% A function to symbolically integrate the Taylor expansion coefficients on
% a triagle mapped to the unit square. 
%
%%
clear all

syms xi eta du1 du2 dv1 dv2 m n

%%
% The mapping to the unit square case. 
%
% The integrand in terms of du_i=u_{i+1}-u_1, and dv_i=v_{i+1}-v_1 the
% coordinate distances of the triangle vertices. 
%
f(eta,m,n,du1,du2,dv1,dv2)=(du1+(du2-du1)*eta)^m *(dv1+(dv2-dv1)*eta)^n...
    /((m+n+2)*factorial(m)*factorial(n));
%
% The order of the expansion.
%
po=4;
%
% Integrate each term and create a function to compute it. 
%
for p=0:po
    for q=0:po
        C(p+1,q+1).C(du1,du2,dv1,dv2)=int(f(eta,p,q,du1,du2,dv1,dv2),eta,[0,1]);
%         matlabFunction(C(p+1,q+1).C(du1,du2,dv1,dv2),'File',['C',num2str(p),num2str(q),'.m']);
    end
end

%%
% The mapping to the standard triangle gives the same result, but we need
% to do both integrals for each value of (m,n). 
%
ft(xi,eta,m,n,du1,du2,dv1,dv2)=((du1*xi+du2*eta)^m *(dv1*xi+dv2*eta)^n)...
    /(factorial(m)*factorial(n));

for p=0:po
    for q=0:po
        tmp(xi,du1,du2,dv1,dv2)=int(ft(xi,eta,p,q,du1,du2,dv1,dv2),eta,[0,1-xi]);
        Ct(p+1,q+1).C(du1,du2,dv1,dv2)=int(tmp(xi,du1,du2,dv1,dv2),xi,[0,1]);
    end
end

C(po,po).C-Ct(po,po).C