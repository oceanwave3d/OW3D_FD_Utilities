function [n,GradPhi,Grad2Phi,m]=ComputeMtermsFD(phi,x,alpha,nu,nv,phin)
%%
% This function takes in a structured grid of points x(1:nu*nv,3) with the
% u-parameter oriented from the stern to the bow and the v-parameter 
% oriented from the keel to the waterline. The points should be ordered to 
% follow v first and then u. The potential on each of these points is phi,
% phin is the normal component of velocity on each point, and alpha is the 
% finite-difference 1/2-stencil size. 
%
% The method described in Bingham and Maniar IWWWFB (1996) is applied, with
% a few of corrections which are noted below. Arbitrary-order
% finite-difference schemes are used to take the u- and v-derivatives.
%
% The functions BuildDx2DOneSided.m, BuildDy2DOneSided.m, and 
% FornbergInterpolate.m should be in the path. 
%
% Written by H.B. Bingham Feb. 18, 2021. Updated 29/09/2024. 
%
%%
N=nu*nv;     % The total number of points on the grid. 
%
% The u,v parameter-space coordinates
%
u=1/(nu-1)*[0:nu-1]';
v=1/nv*(1/2+[0:nv-1]');
%
% The derivative matrices ordered to follow v-first then u.
%
Du=BuildDx2DOneSided(nu,nv,u,alpha,1);
Dv=BuildDy2DOneSided(nu,nv,v,alpha,1);
%
% Take all of the coordinate derivatives and build the operators in Eqs.
% (8) & (9) of Bingham & Maniar (1996).
%
xu(:,1)=Du*x(:,1); xu(:,2)=Du*x(:,2); xu(:,3)=Du*x(:,3);
xv(:,1)=Dv*x(:,1); xv(:,2)=Dv*x(:,2); xv(:,3)=Dv*x(:,3);
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    n(i,1)=-(xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2));
    n(i,2)=-(xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3));
    n(i,3)=-(xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1));
end
H=sqrt(E.*G-F.^2); % The Jacobian
for i=1:3
    n(:,i)=n(:,i)./H; % The normal vector pointing into the body. 
end
%
% Take the surface derivatives of phi.
%
phiu=Du*phi; phiv=Dv*phi;
%
% Build the physical gradient using Eq. (8). 
%
for j=1:3
    GradPhi(:,j)=1./H.^2.*(xu(:,j).*(G.*phiu-F.*phiv) + xv(:,j).*(E.*phiv-F.*phiu)) ...
        + phin.*n(:,j);
end
%
% Compute the surface gradient tensor and the surface divergence of the 
% velocity field.
%
DivSu=zeros(N,1); GradSu=zeros(N,3,3);
for j=1:3
    phiu=Du*GradPhi(:,j); phiv=Dv*GradPhi(:,j);
    for i=1:3
        GradSu(:,i,j)=1./H.^2.*(xu(:,i).*(G.*phiu-F.*phiv) + xv(:,i).*(E.*phiv-F.*phiu));
    end
    DivSu=DivSu+GradSu(:,j,j);
end
%
% Compute the m-terms from Eq. (11). What's written in the paper as 
% n^2\dot GradS GradPhi, might more natrurally be written simply as the
% product between the surface gradient matrix and the normal vector as a
% column-vector. 
%
for i=1:3
    tmp=zeros(N,1);
    for j=1:3
        tmp=tmp+n(:,j).*GradSu(:,i,j);
    end
    m(:,i)=-(tmp - n(:,i).*DivSu);
end
%
% The rotational m-terms from Eq. (12). There is an error here in the
% published abstract, it should be: m_k = - n X W + x X m. 
%
m(:,4)=-(n(:,2).*GradPhi(:,3)-n(:,3).*GradPhi(:,2)) + x(:,2).*m(:,3)-x(:,3).*m(:,2);
m(:,5)=-(n(:,3).*GradPhi(:,1)-n(:,1).*GradPhi(:,3)) + x(:,3).*m(:,1)-x(:,1).*m(:,3);
m(:,6)=-(n(:,1).*GradPhi(:,2)-n(:,2).*GradPhi(:,1)) + x(:,1).*m(:,2)-x(:,2).*m(:,1);
%
% The second-derivatives computed from Eq. (13). Here we also have one
% error in the equation: The second term should be minus. 
%
ind=0; Grad2Phi=zeros(N,6);
for i=1:3
    for j=i:3
        ind=ind+1;
        Grad2Phi(:,ind)= GradSu(:,i,j) - n(:,i).*m(:,j);
    end
end
end