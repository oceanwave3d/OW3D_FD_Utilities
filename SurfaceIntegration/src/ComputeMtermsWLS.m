function [n,GradPhi,Grad2Phi,m]=ComputeMtermsWLS(x,uv,p,phi,phin)
%%
% This function takes a scattered set of physical points x(1:N,1:3) mapped
% to the parameter space uv(1:N,1:2) and computes the first- and
% second-gradients of phi(1:N,1) given the normal derivative phin(1:N,1),
% along with the six m-terms. An order-p WLS derivative operator is used
% together with the method described in Bingham and Maniar IWWWFB (1996)
% with two sign corrections. The u-v space should follow the x- and
% z-coordinates to ensure an inward pointing normal vector to the body
% surface. 
%
% The functions BuildLaplaceOperators_WLS_2D.m and FDInterpolate2D_WLS.m 
% must be in the path. 
%
% Written by H.B. Bingham, DTU, Sept. 29, 2024.
%
%%
%
% Set up the WLS parameters.
%
N=size(x,1);
% p is the expansion order of the WLS operators
switch p
    case 2
        sigma=.75;
    case 3
        sigma=1;
    case 4
        sigma=1.2;
    case 5
        sigma=1.4;
    case 6
        sigma=1.8;
end
% The stencil size. np=(p+3)^2 seems to be the minimum to give a 
% well-conditioned system.
np=(p+3)^2;    % Total number of points in the stencil
% Wtype='Unit'; sigma=1;
Wtype='Gaussian'; Wmin=0.000001;
%%
% Use KDTree to find np nearest-neighbors to each point (the WLS stencil)
%
% To ensure a roughly even number of points in each direction, scale the
% (u,v)-space accordingly for the neighbor search. 
%
kdtreeNS = KDTreeSearcher(uv);
[StencilMap,dist]=knnsearch(kdtreeNS,uv,'K',np);
%
D.n=N; D.p=p; D.np=np; D.Wtype=Wtype; D.sigma=sigma; D.Wmin=Wmin; 
D=BuildLaplaceOperators_WLS_2D(uv,StencilMap,uv,D);
Du=D.Dx; Dv=D.Dz; 
%
% Take all of the coordinate derivatives and build the operators in Eqs.
% (8) & (9) of Bingham & Maniar (1996).
%
xu(:,1)=Du*x(:,1); xu(:,2)=Du*x(:,2); xu(:,3)=Du*x(:,3);
xv(:,1)=Dv*x(:,1); xv(:,2)=Dv*x(:,2); xv(:,3)=Dv*x(:,3);
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    n(i,1)=-(xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2));
    n(i,2)=-(xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3));
    n(i,3)=-(xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1));
end
H=sqrt(E.*G-F.^2); % The Jacobian
for i=1:3
    n(:,i)=n(:,i)./H; % The normal vector pointing into the body. 
end
%
% Take the surface derivatives of phi.
%
phiu=Du*phi; phiv=Dv*phi;
%
% Build the physical gradient
%
for j=1:3
    GradPhi(:,j)=1./H.^2.*(xu(:,j).*(G.*phiu-F.*phiv) + xv(:,j).*(E.*phiv-F.*phiu)) ...
        + phin.*n(:,j);
end
%
% Compute the surface gradient tensor and the surface divergence of the 
% velocity field
%
DivSu=zeros(N,1); GradSu=zeros(N,3,3);
for j=1:3
    phiu=Du*GradPhi(:,j); phiv=Dv*GradPhi(:,j);
    for i=1:3
        GradSu(:,i,j)=1./H.^2.*(xu(:,i).*(G.*phiu-F.*phiv) + xv(:,i).*(E.*phiv-F.*phiu));
    end
    DivSu=DivSu+GradSu(:,j,j);
end
%
% Compute the m-terms from Eq. (11). Note that the first term here appears
% to be defined backwards in the paper, i.e. it should be the product
% between the normal vector (as a row vector) and the surface gradient 
% matrix. 
%
for i=1:3
    tmp=zeros(N,1);
    for j=1:3
        tmp=tmp+n(:,j).*GradSu(:,i,j);
    end
    m(:,i)=-(tmp - n(:,i).*DivSu);
end
%
% The second-derivatives computed from Eq. (13). Here we also have one 
% error: the second term should be minus. 
%
ind=0; Grad2Phi=zeros(N,6);
for i=1:3
    for j=i:3
        ind=ind+1;
        Grad2Phi(:,ind)= GradSu(:,i,j) - n(:,i).*m(:,j);
    end
end
