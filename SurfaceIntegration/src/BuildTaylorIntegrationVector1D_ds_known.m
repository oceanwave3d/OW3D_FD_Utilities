function Ivec=BuildTaylorIntegrationVector1D_ds_known(s,p,ds)
%%
% This function builds the Taylor integration vector which, when multiplied
% by the a vector of function values, gives the integral over the line of 
% the function.
%
% s(1:n) holds the arc-length positions where the function 
% is known, p+1 is the number of Taylor series terms to include in the 
% expansions and  ds(1:n-1) are the arc-lengths of each interval. 
%
% More detailed discussion can be found in Sec. 4.2 of: 
%
% "A note on finite differences in multiple-domain boundary-fitted 
% coordinates" 
%  by
% H. B. Bingham, DTU, 2024. 
%
% The integral from s=s_j to s=s_{j+1} along the arc of length ds_j in 
% physical space is:  
%
%  I_0=sum_{n=0}^p ds_j^{n+1}/(n+1)! f_j^{(n)}.  
%
% Adding these sub-integrals from j=1:n-1 gives the total integral. 
%
% This operation can be expressed as the dot product between the
% integration row vector Ivec(1,1:n) and the function value vector 
% f(1:n,1), where Ivec is found from the product 
%
%  Ivec = I(1,1:n-1) * A(1:n-1,1:n).
%
% Here I is the unit row vector and A is the matrix of Taylor expansion
% integral operators. 
%
%%
% Written by H. B. Bingham, DTU, 2020.
% This version: March., 2024
%
%%
n=length(s);
%
% Build all the required derivative matrices
%
alpha=p/2; 
for iorder=1:p
    Ds(iorder).D=BuildDx1DOneSided(n,s,alpha,iorder);
end
%
% Build the matrix of sub-integral derivative operators
%
A=spdiags(ds,0,n-1,n);
for ip=1:n-1
    for iorder=1:p
        A(ip,:)=A(ip,:)+ds(ip)^(iorder+1)/factorial(iorder+1)*Ds(iorder).D(ip,:);
    end
end
%
% Multiply with the unit vector to sum all columns and
% build the integration vector.
%
I=ones(1,n-1);
Ivec=I*A;

