function D=BuildLaplaceOperators_WLS_2D(x,StencilMap,x0,D)
       
% BuildLaplaceOperators_WLS_2D:  Build order-p, np-point first- and
% second-derivative operators on the scattered grid x(1:n,1:2) using the
% WLS method. StencilMap(1:n,1:np) gives the grid point numbers of each
% stencil and x0 are the evaluation points for the operator for each x
% point. 
%
% The structure D should be passed in with: 
% D.n     - number of grid points
% D.p     - the order of the 1st-derivative scheme
% D.np    - number of points in the stencil
% D.Wtype - either 'Unit' or 'Gaussian' type weight function
% D.sigma - the Gaussian width parameter for Wtype='Gaussian'
%
% A lower limit on the weighting for the Gaussian weight function is
% applied, and when this is chosen this should be passed in as 
%
% D.Wmin 
%
% D.Wmin=0.01 - 10^6 seems to be a good choice depending on the grid. 
%
% On return, the structure will hold the derivative operators: 
% D.Dx, D.Dz, D.L
%
% If any points lead to nearly singular matrices, then they will be listed
% in 
%
% D.isingular
%
% Written by H.B. Bingham, DTU, 2014-2018
%
% Initialize the operators and variables
%
n=D.n; p=D.p; np=D.np; ncol=length(x);
D.Dx=sparse(n,ncol); D.Dz=sparse(n,ncol); D.L=sparse(n,ncol); 
nTaylor=(p+1)^2;
c=zeros(nTaylor,np); 
%
ierr=0; D.isingular=[];
for i=1:n
    errflag=0;
    xp(1:np,1:2)=x(StencilMap(i,:),1:2); xp0=[x0(i,1),x0(i,2)];
    d=sqrt((xp(:,1)-xp0(1)).^2+(xp(:,2)-xp0(2)).^2);
    in= d~=0; tmp=sort(d(in)); dist=mean(tmp(1:p+1)); %dist=min(d(in)); 
    s=D.sigma*dist;
    switch D.Wtype
        case 'Gaussian'
            w=exp(-d.^2/s^2);
            w(w<=D.Wmin)=D.Wmin;  % Limit the minimum value of the weight
        otherwise
            w=ones(np,1);
    end
    [c(:,1:np),errflag]=FDInterpolate2D_WLS(xp0,xp,p,w,dist);
    if errflag ~= 0
        ierr=ierr+1; D.isingular(ierr)=i;
    end
    D.Dx(i,StencilMap(i,:))=c(p+2,1:np);
    D.Dz(i,StencilMap(i,:))=c(2,1:np);
    D.L(i,StencilMap(i,:))=c(2*(p+1)+1,1:np)+c(3,1:np);
end

end

