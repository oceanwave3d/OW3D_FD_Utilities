function Ifd=TaylorIntegrate2D_Direct(f,D)
%
% Given the sparse derivative matrices D(1:p+1,1:p+1).D which have been set
% up in the function TaylorIntegration2D_Direct_SetUp.m, compute the
% integral of the function f(1:N,1) over the surface of the body. 
%
% Written by H.B. Bingham, DTU, June, 2022.
%
%%
N=size(f,1);    % The number of grid points.
p=size(D,1)-1;  % The order of the scheme. 
nu=D(1,1).nu; nv=D(1,1).nv;  % The (u,v)-space discretization. 
du=D(1,1).du; dv=D(1,1).dv;
%
% Take all the required (u,v) derivatives of the function.
%
fx=zeros(N,p+1,p+1);
for m=1:p+1
    for n=1:p+1
        fx(:,m,n) = D(m,n).D*f;
    end
end
%
% Build the index map for the bottom left corner points.
%
kj=ones(nv-1,1)*[nv*([1:nu-1]-1)]+[1:nv-1]'*ones(1,nu-1); kj=kj(:);
%
% Collect the integral contributions.
%
Ifd=0;
for m=1:p+1
    for n=1:p+1
        Ifd=Ifd+sum(du.^(m)./factorial(m).*dv.^(n)./factorial(n).*...
            fx(kj,m,n));
    end
end
end
