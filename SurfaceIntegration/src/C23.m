function out1 = C23(du1,du2,dv1,dv2)
%C23
%    OUT1 = C23(DU1,DU2,DV1,DV2)

%    This function was generated by the Symbolic Math Toolbox version 8.5.
%    19-Jun-2022 19:16:20

t2 = du1.^2;
t3 = du2.^2;
t4 = dv1.^2;
t5 = dv1.^3;
t6 = dv2.^2;
t7 = dv2.^3;
out1 = (t2.*t5)./5.04e+2+(t3.*t5)./5.04e+3+(t2.*t7)./5.04e+3+(t3.*t7)./5.04e+2+(du1.*du2.*t5)./1.26e+3+(du1.*du2.*t7)./1.26e+3+(dv2.*t2.*t4)./8.4e+2+(dv1.*t2.*t6)./1.68e+3+(dv2.*t3.*t4)./1.68e+3+(dv1.*t3.*t6)./8.4e+2+(du1.*du2.*dv2.*t4)./8.4e+2+(du1.*du2.*dv1.*t6)./8.4e+2;
