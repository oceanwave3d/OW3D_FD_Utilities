function out1 = C11(du1,du2,dv1,dv2)
%C11
%    OUT1 = C11(DU1,DU2,DV1,DV2)

%    This function was generated by the Symbolic Math Toolbox version 8.5.
%    19-Jun-2022 19:16:18

out1 = (du1.*dv1)./1.2e+1+(du1.*dv2)./2.4e+1+(du2.*dv1)./2.4e+1+(du2.*dv2)./1.2e+1;
