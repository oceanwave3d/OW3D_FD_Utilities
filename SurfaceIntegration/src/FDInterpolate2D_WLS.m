function [c,errflag]=FDInterpolate2D_WLS(x0,x,p,w,dx)
%
% Written by Harry B. Bingham (2016)       --This version    14.03.2018
%
% A function to pass a polynomial interpolant through the np 
% points at positions x(1:np,1:2) and evaluate the function plus it's 
% up to order p derivatives (p+1 Taylor coefficients) at the position 
% x0(1:2).  dx is a scale factor for the coefficients which is a 
% representative grid spacing. w(1:np) is the weight function for each
% stencil point. 
%
% The derivatives are ordered following Pascals triangle with y-derivatives
% in the inner loop.
%
% Initialize the error flag
errflag=0;
%
% Build the scaled, Taylor coefficient matrix:  
%
nTaylor=(p+1)^2;
fact=1/dx; 
np=length(x(:,1));  % Number of stencil points
W=diag(w);          % The diagonal weight matrix
if np<nTaylor
    error(['np must be >= (p+1)^2 for this WLS interpolation']); 
    stop; 
end
%
jm=0;
for m=0:p  % x-derivatives
    for n=0:p % y-derivatives
        jm=jm+1;
        for im=1:np
            mat(im,jm)=(fact*(x(im,1)-x0(1)))^m/factorial(m) ...
                *(fact*(x(im,2)-x0(2)))^n/factorial(n);
        end
    end
end
%
% Solve for the coefficients
%
Mat=mat'*W*mat;
c= inv(Mat)*mat'*W;
%
% Trap for a nearly singular matrix
%
rCond=rcond(Mat);
if rCond <= 1.e-15
    errflag=1;
end
%
% Re-scale the coefficients
%
im=0;
for m=0:p
    for n=0:p
        im=im+1; c(im,:)=fact^m*fact^n*c(im,:); 
    end 
end
end