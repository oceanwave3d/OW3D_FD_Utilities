function [Ivec,nvec]=BuildTaylorIntegrationVector2D(x,u,v,p)
%%
% This function builds the 2D Taylor integration vector which, when 
% multiplied by a vector of function values, gives the integral over the 
% body surface of the function. The surface is defined by a structured grid
% of points so that standard finite differences are used in the Taylor
% expansion. 
%
% More detailed discussion can be found in: 
%
% "A note on finite differences in multiple-domain boundary-fitted 
% coordinates" 
%  by
% H. B. Bingham, DTU, 2024. 
% 
% This version: Oct. 5, 2024.
%
%%
% The physical coordinates of the surface are input in the matrix:
% 
%        x(1:N,1:3)
%
% and the parameter space is: 
%
%        u(1:nu,1), v(1:nv,1)
%
% where the indices of x(1:N,1) follow v first then u. I.e. index k is:
%
%        for i=1:nu
%            for j=1:nv
%                k=(i-1)*nv+j
%            end
%        end
% 
% with N=nu*nv. 
%
%        p = the order of the Taylor series integration. 
%
% The function returns: 
%
%        Ivec(1,1:N)         The integration row vector.
%        nvec(1:N,1:3)       The unit normal vectors to each grid point x.
%
% To integrate an arbitrary function f(u,v) over the surface, we compute: 
%
%        Integral = Ivec*f(1:N,1);
%
%
%%
N=size(x,1); nu=length(u); nv=length(v); % The size of the (u,v)-space
tmp=diff(u); du=repmat(tmp,[nv-1,1]); du=du(:); % The (u,v) grid spacing.
tmp=diff(v); dv=repmat(tmp',[1,nu-1]); dv=dv(:);

%
% Build all the required derivative matrices
%
alpha=p/2; 
%%
% Build the required 2D derivative operators for Taylor Integration.
%
D(1,1).D=speye(N,N);
for m=1:p
    D(m+1,1).D=BuildDx2DOneSided(nu,nv,u,alpha,m);
    D(1,m+1).D=BuildDy2DOneSided(nu,nv,v,alpha,m);
end
for m=2:p+1
    for n=2:p+1
        D(m,n).D=D(m,1).D*D(1,n).D;
    end
end
%
% Take the coordinate derivatives to compute the normal vectors and 
% the elements of surface area.
%
xu(:,1)=D(2,1).D*x(:,1); xu(:,2)=D(2,1).D*x(:,2); xu(:,3)=D(2,1).D*x(:,3);
xv(:,1)=D(1,2).D*x(:,1); xv(:,2)=D(1,2).D*x(:,2); xv(:,3)=D(1,2).D*x(:,3);
E=zeros(N,1); F=E; G=E; H=E; nvec=zeros(N,3);  % Initialize
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    nvec(i,1)=-(xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2));
    nvec(i,2)=-(xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3));
    nvec(i,3)=-(xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1));
end
H=sqrt(E.*G-F.^2); % The Jacobian (which is also the element of surface area.)
for i=1:3
    nvec(:,i)=nvec(:,i)./H; % The unit normal vector pointing into the body.
end
%
% Build the integration vector from the Taylor expansion coefficients. 
%
% The index map for the bottom left corner points in u-v space.
%
nel=(nu-1)*(nv-1); 
kj=ones(nv-1,1)*[nv*([1:nu-1]-1)]+[1:nv-1]'*ones(1,nu-1); kj=kj(:);
%
Ivec=zeros(1,N);
for m=1:p+1
    for n=1:p+1
        Ivec=Ivec+(du.^m./factorial(m).*dv.^n./factorial(n))'*D(m,n).D(kj,:);
    end
end
% 
% Scale by the elements of area and return. 
%
Ivec=Ivec.*H'; 
end
