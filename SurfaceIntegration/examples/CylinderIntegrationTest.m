%
% This script looks at the convergence of 2D Taylor integration over a
% vertical axis circular cylinder. We consider the calculation of the body
% surface area and volume and show that the method converges at roughly
% order p when p-order Taylor expansions are applied. 
%
% Written by H.B. Bingham, DTU, June 15, 2022. Updated to orient the u-v
% space correctly on Oct. 5, 2024. 
%
%%
addpath ../src
%%
%%
clear all
ifig=0;
%
% Choose the direct or the indirect method. In the direct method, all
% required derivatives are taken explicitly for the given function, then
% they are summed via Taylor integration to give the final result. In the
% indirect case, we build the integration vector for a given grid and
% expansion order p, then apply it to the function to get the integral. The
% indirect method is more elegant and more efficient. 
%
% Method='Direct';          
Method='Indirect';      
 
%%
% Plot one example of grid spacing and expansion order p.
%
p=4; % The order of the schemes
alpha=p/2; r=2*alpha+1;
%
% The number of grid points.
%
nu=10; nv=nu-1;
du=1/(nu-1); dv=1/(nv-1);
u=du*[0:nu-1]'; v=dv*[0:nv-1]';
[U,V]=meshgrid(u,v);
N=nu*nv; % The total number of grid points.
%
% Define the geometry. Here a quarter of a bottom-mounted circular cylinder
%
R=1; h0=R;
xp=zeros(nv,nu); yp=xp; zp=xp;
xp=R*sin(pi/2*U); yp=R*cos(pi/2*U); zp=h0*(-1+V);
%
% Plot the geometry.
%
ifig=ifig+1; figure(ifig);clf; hold on
plot3(xp,yp,zp,'o-')
%
% Put the coordinates into a vector.
%
x=[xp(:) yp(:) zp(:)];

switch Method
    case 'Direct'
        [D, nvec, ds]=TaylorIntegration2D_Direct_SetUp(x,u,v,p);
    case 'Indirect'
        [Ivec,nvec]=BuildTaylorIntegrationVector2D(x,u,v,p);
end
%
% Add the normal vectors to the plot.
%
figure(ifig); hold on;
quiver3(x(:,1), x(:,2), x(:,3), nvec(:,1), nvec(:,2),nvec(:,3),...
    'MarkerSize', 5); axis equal;
%
% Compute the surface area and volume.
%
switch Method
    case 'Direct'
        f=ds;
        S=TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,1).*x(:,1).*ds;
        V1=TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,2).*x(:,2).*ds;
        V2=TaylorIntegrate2D_Direct(f,D);
    case 'Indirect'
        S=Ivec*ones(N,1);
        V1=-Ivec*(nvec(:,1).*x(:,1));
        V2=-Ivec*(nvec(:,2).*x(:,2));
end
Sex=2*pi*R*h0/4;
display(['Relative error in the surface area: ',num2str(abs((S-Sex)/Sex))])
V=(V1+V2)/2;
Vex=pi*R^2*h0/4;
display(['Relative error in the volume: ',num2str(abs((V-Vex)/Vex))])

%%
% Do a convergence analysis of the surface area and volume calculations. 
%
% Choose the expansion orders and grid spacings to consider: 
%
pvec=[2 4 6 8]; nuvec=[10 20 40 80]; 
R=1; h0=R;
Sex=2*pi*R*h0/4;
Vex=pi*R^2*h0/4;
%
% Loop over each combination and collect the errors.
%
for io=1:length(pvec)
    p=pvec(io);
    alpha=p/2; r=2*alpha+1;
    for in=1:length(nuvec)
        nu=nuvec(in); nv=nu;
        du=1/(nu-1); dv=1/(nv-1);
        u=du*[0:nu-1]';  v=dv*[0:nv-1]';
        N=nu*nv; 
        th=pi/2*[0:nu-1]/(nu-1); z=h0*(-1+[0:nv-1]/(nv-1));
        [Th,H]=meshgrid(th,z);
        xp=zeros(nv,nu); yp=xp; zp=xp;
        xp=R*sin(Th); yp=R*cos(Th); zp=H;
        x=zeros(N,3); x=[xp(:) yp(:) zp(:)];
        Ivec=zeros(N,1); nvec=zeros(N,3);
        switch Method
            case 'Direct'
                [D, nvec, ds]=TaylorIntegration2D_Direct_SetUp(x,u,v,p);
                f=ds;
                S=TaylorIntegrate2D_Direct(f,D);
                f=-nvec(:,1).*x(:,1).*ds;
                V1=TaylorIntegrate2D_Direct(f,D);
                f=-nvec(:,2).*x(:,2).*ds;
                V2=TaylorIntegrate2D_Direct(f,D);
            case 'Indirect'
                [Ivec,nvec]=BuildTaylorIntegrationVector2D(x,u,v,p);
                S=Ivec*ones(N,1);
                V1=-Ivec*(nvec(:,1).*x(:,1));
                V2=-Ivec*(nvec(:,2).*x(:,2));
        end
        errS(in,io)=abs((S-Sex)/Sex);
        V=(V1+V2)/2;
        errV(in,io)=abs((V-Vex)/Vex);
    end
end
%
% Plot the results
%
ifig=ifig+1; figure(ifig);clf
loglog(nuvec,errS(:,1),'-+',nuvec,errS(:,2),'-o',nuvec,errS(:,3),'-*',...
    nuvec,errS(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in S');
b2=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errS(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on
title(['A circular cylinder']);

ifig=ifig+1; figure(ifig);clf
loglog(nuvec,errV(:,1),'-+',nuvec,errV(:,2),'-o',nuvec,errV(:,3),'-*',...
    nuvec,errV(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in V');
b2=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errV(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on
title(['A circular cylinder']);

