%%
%  This is just a sanity test to check that we can compute the surface area
%  and volume of a barge when it's represented by 4 patches describing the
%  half-body in the positive y-domain. As expected, it works fine and gives
%  errors around 10^-10 for the Gauss method and 10^-14 for the Taylor
%  method. 
%  
%  Either Taylor or Gauss integration can be used. 
%
% Written by H.B. Bingham, DTU, 2023
% This version: October 7, 2024
%
%%
addpath ../src/
%
%%
clearvars
ifig=0;
%% Integration method
% Choose Taylor or Gauss integration
%
% Method='Gauss';
Method='Taylor';
%
%% One example of expansion order and discretization.
%
% Set up the WLS parameters.
%
p=2;  % Expansion order of the WLS operators
switch p
    case 2
        sigma=.75;
    case 3
        sigma=1;
    case 4
        sigma=1.2;
    case 5
        sigma=1.4;
    case 6
        sigma=1.8;
end
% The stencil size. np=(p+3)^2 seems to be the minimum to give a 
% well-conditioned system.
np=(p+3)^2;    % Total number of points in the stencil
% Wtype='Unit'; sigma=1;
Wtype='Gaussian'; Wmin=0.000001;
%%
% The scattered point set on the half barge. Note that we need at least np
% grid points on each patch for this to work. 
%
L=1; B=L/10; T=L/16; 
nx=9; ny=(nx+1)/2; nz=(nx+1)/2; 
%
% The hull points as 4 patches along with each u-v space.
%
dx=L/(nx-1); dy=B/(ny-1)/2; dz=T/(nz-1); 
%
npatch=4;
%
% Patchs 1 & 4, the bow and stern
%
[Y,Z]=meshgrid([0:ny-1]*dy,-T+[0:nz-1]*dz); N=ny*nz;
x(1).N=N;
x(1).x(1:N,1)=L/2; x(1).x(1:N,2)=Y(:); x(1).x(1:N,3)=Z(:);
% u,v space
x(1).uv(:,1)=-1+4*x(1).x(:,2)/B; x(1).uv(:,2)=-1-2*x(1).x(:,3)/T;
% Triangulate the points with Delaunay triangulation. 
tmp=delaunayTriangulation(x(1).uv);
x(1).t=tmp.ConnectivityList;
x(1).nel=size(x(1).t,1);
%
x(4).N=N;
x(4).x(1:N,1)=-L/2; x(4).x(1:N,2)=Y(:); x(4).x(1:N,3)=Z(:);
% u,v space
x(4).uv(:,1)=-1+4*x(4).x(:,2)/B; x(4).uv(:,2)=1+2*x(1).x(:,3)/T;
% Triangulate the points with Delaunay triangulation. 
tmp=delaunayTriangulation(x(4).uv);
x(4).t=tmp.ConnectivityList;
x(4).nel=size(x(4).t,1);
%
% Patch 2, the side
%
[X,Z]=meshgrid(-L/2+[0:nx-1]*dx,-T+[0:nz-1]*dz); N=nx*nz;
x(2).N=N;
x(2).x(1:N,1)=X(:); x(2).x(1:N,2)=B/2; x(2).x(1:N,3)=Z(:);
% u,v space
x(2).uv(:,1)=2*x(2).x(:,1)/L; x(2).uv(:,2)=1+2*x(2).x(:,3)/T;
% Triangulate the points with Delaunay triangulation. 
tmp=delaunayTriangulation(x(2).uv);
x(2).t=tmp.ConnectivityList;
x(2).nel=size(x(2).t,1);
%
% Patch 3, the bottom
%
[X,Y]=meshgrid(-L/2+[0:nx-1]*dx,[0:nz-1]*dy); N=nx*ny;
x(3).N=N;
x(3).x(1:N,1)=X(:); x(3).x(1:N,2)=Y(:); x(3).x(1:N,3)=-T;
% u,v space
x(3).uv(:,1)=2*x(3).x(:,1)/L; x(3).uv(:,2)=-1+4*x(3).x(:,2)/B;
% Triangulate the points with Delaunay triangulation. 
tmp=delaunayTriangulation(x(3).uv);
x(3).t=tmp.ConnectivityList;
x(3).nel=size(x(3).t,1);
%%
% Use KDTree to find np nearest-neighbors to each point (the WLS stencil)
%
% To ensure a roughly even number of points in each direction, scale the
% (u,v)-space accordingly for the neighbor search. 
%
ifig=ifig+1; figure(ifig);clf; hold on;
for i=1:npatch
    clear('xv','uv','t','D','nvec','ds');

    xv=x(i).x; uv=x(i).uv; t=x(i).t; nel=x(i).nel;
    switch i
        case {1,4}
            n1=ny; n2=nz;
        case 2
            n1=nx; n2=nz;
        case 3
            n1=nx; n2=ny;
    end
    Lu=max(uv(:,1))-min(uv(:,1)); Lv=max(uv(:,2))-min(uv(:,2));
    fac=n1/n2 * Lv/Lu;
    uvs=[uv(:,1),uv(:,2)/fac]; 
    %
    kdtreeNS = KDTreeSearcher(uvs);
    [StencilMap,dist]=knnsearch(kdtreeNS,uvs,'K',np);
    %%
    % Set up the surface gradient and integration operators and find the normal
    % vectors and elements of surface area for each grid point and element.
    %
    D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
    D(1,1).Wmin=Wmin;

    switch Method
        case 'Taylor'
            [D, nvec]=TaylorIntegration2D_WLS_SetUp(xv,uv,StencilMap,t,D);
        case 'Gauss'
            [D, nvec]=GaussianIntegration2D_WLS_SetUp(xv,uv,StencilMap,t,D);
    end
    x(i).D=D; x(i).nvec=nvec; 
    %
    % Plot the 3D surface
    %
    
    for iv=1:nel
        plot3(xv(t(iv,[1:3,1]),1),xv(t(iv,[1:3,1]),2),xv(t(iv,[1:3,1]),3),'-ok');
    end
    xlabel('x'); ylabel('y'); zlabel('z')
    fac=1/(30*L);
    quiver3(xv(:,1), xv(:,2), xv(:,3), fac*nvec(:,1), fac*nvec(:,2),fac*nvec(:,3),...
        'AutoScale','off'); axis equal; grid on
    view(100,-16);
end
%%
% Integrate to find the surface area and volume. 
%
Sex=L*(2*T+B)+2*B*T;
Vex=L*B*T;
S=0; V1=0; V2=0; V3=0;
for i=1:npatch
    clear('f','f1','f2','f3');
    f=ones(x(i).N,1); f1=-x(i).nvec(:,1).*x(i).x(:,1);
    f2=-x(i).nvec(:,2).*x(i).x(:,2); 
    f3=-x(i).nvec(:,3).*x(i).x(:,3);


    S=S+x(i).D(1,1).Ivec*f;
    V1=V1+x(i).D(1,1).Ivec*f1;
    V2=V2+x(i).D(1,1).Ivec*f2;
    V3=V3+x(i).D(1,1).Ivec*f3;
end
S=2*S; 


display(['Relative error in the surface area: ',num2str(abs(S-Sex)/Sex)])

Vol=2*(V1+V2+V3)/3;

display(['Relative error in the volume: ',num2str(abs((Vol-Vex)/Vex))])

