%%
%  This example defines a grid of triangles in the physical space on a 1/2
%  hemisphere. Several ways to map the physical coordinates to the (u,v)
%  parameter-space are tested, and the best seems to be to project the
%  points based on their angles in the x-y and y-z planes to a
%  rectangular (u,v)-space. This gives convergence of the surface area and
%  volume calculations, though the surface area calculation only reaches
%  2nd-order. 
%
%  Mapping to the half-circle seems more natural, and works to some extent,
%  but it leads to singular WLS matrices for fine grids and/or high p. 
%
% Written by H.B. Bingham, DTU
% This version: March 2, 2024
%
%%
addpath ../src/
%
%%
clear all
ifig=1;
%% Choose the mapping type and the (u,v)-space boundary.
% 'Tutte' follows the method described by Hormann et al "Mesh
% Parameterization: Theory and Practice" 2007 (see /docs), with unit
% 'spring constant'. This is a work in progress and not recommended yet. 
%
% 'Stereo' projects the points based on their angles in the x-y, and y-z
% planes respectively. 
%
% Mapping='Tutte';
Mapping='Stereo';
%
% For either method, the boundary can either be mapped to the 1/2 circle or
% the rectangle in the (u,v)-plane. 
%
% Boundary='Circle';
Boundary='Rectangle';

%% Convergence test or not. This is only run for the rectangular boundary, stereographic projection case. 
%
% Convergence='no';
Convergence='yes';
%
%% One example of expansion order and discretization.
%
% Set up the WLS parameters.
%
p=4;  % Expansion order of the WLS operators
switch p
    case 2
        sigma=.75;
    case 3
        sigma=1;
    case 4
        sigma=1;
    case 5
        sigma=1.4;
    case 6
        sigma=1.8;
end
% The stencil size. np=(p+3)^2 seems to be the minimum to give a 
% well-conditioned system.
np=(p+3)^2;    % Total number of points in the stencil
% Wtype='Unit'; sigma=1;
Wtype='Gaussian'; Wmin=0.000001;
%%
% The scattered point set on the half hemisphere
%
R=1; 
nth=11; nz=(nth+1)/2; 
%
% The FS boundary points
%
dth=pi/(nth-1); dph=pi/2/(nz-1);
x(1:nth,1)=R*cos(-pi+[0:nth-1]*dth);
x(1:nth,2)=-R*sin(-pi+[0:nth-1]*dth);
x(1:nth,3)=0;
%
% Then the y=0 boundary points (without duplicates)
%
x(nth+1:2*nth-2,1)=R*cos([1:nth-2]*dth);
x(nth+1:2*nth-2,2)=0;
x(nth+1:2*nth-2,3)=-sin([1:nth-2]*dth);
%
% Fill in the rest of the physical points.
%
id=length(x); nb=id; % The number of boundary points.
for j=2:nz-1
    x(id+1:id+nth-2,1)=R*cos((j-1)*dph)*cos(-pi+[1:nth-2]*dth);
    x(id+1:id+nth-2,2)=-R*cos((j-1)*dph)*sin(-pi+[1:nth-2]*dth);
    x(id+1:id+nth-2,3)=-R*sin((j-1)*dph);
    id=id+nth-2;
end

N=size(x,1); % The total number of points
n=N-nb;      % The number of interior points

%%
%
% Map the physical space to the (u,v) parameter space. 
% 
switch Mapping
    case 'Tutte'
        %
        % Triangulate in the x-space using Delaunay triangulation.
        %
        tmp=delaunayTriangulation(x(:,1:2));
        t=tmp.ConnectivityList;
        nel=size(t,1);

        switch Boundary
            case 'Circle'
                %
                % Take the boundary points to be (u,v)=(x,-z)/R
                %
                uv(:,1)=x(:,1)/R; uv(:,2)=x(:,3)/R;
                %
                % Or, take a uniform spacing along the FS
                %
                uv(1:nth,1)=-1+2*[0:nth-1]/(nth-1);
            case 'Rectangle'
                r=sqrt(x(:,1).^2+x(:,2).^2)/R; th=atan2(x(:,2),x(:,1));
                ph=acos(-x(:,3)/R);
                uv(:,1)=-2*(-.5+th./pi); uv(:,2)=2*ph/pi;
                uv(nth+nz-1,1)=0; % Correct the pole point u-position.
        end
        %
        % Build the connectivity matrix between nodes (unscaled).
        %
        A=sparse(N,N);
        for i=1:nel
            A(t(i,1),t(i,2))=1; A(t(i,1),t(i,3))=1; A(t(i,2),t(i,3))=1;
            A(t(i,2),t(i,1))=1; A(t(i,3),t(i,1))=1; A(t(i,3),t(i,2))=1;
        end
        %
        % Now scale each row so that the off-diagonals sum to one and place a one
        % on the diagonal.
        %
        for i=1:N
            d=sum(A(i,:));
            A(i,:)=-A(i,:)/d; A(i,i)=1;
        end
        %
        % Split the matrix into boundary and interior point contributions
        %
        An=A(nb+1:N,nb+1:N); B=A(nb+1:N,1:nb);
        %
        % Solve for the interior point positions in the u,v space.
        %
        rhs=-B*uv(1:nb,1);
        uv(nb+1:N,1)=An\rhs;
        
        rhs=-B*uv(1:nb,2);
        uv(nb+1:N,2)=An\rhs;
    case 'Stereo'
        %
        % Stereographic projection (I think?) Or maybe equal arc-length? 
        %
        r=sqrt(x(:,1).^2+x(:,2).^2)/R; th=atan2(x(:,2),x(:,1));
        ph=acos(-x(:,3)/R);
        switch Boundary
            case 'Circle'
                uv(:,1)=-2*r.*(-.5+th./pi); 
                uv(:,2)=1+x(:,3)/R; % z
%                 uv(:,2)=2*ph/pi;
            case 'Rectangle'
                uv(:,1)=-2*(-.5+th./pi); uv(:,2)=2*ph/pi;
%                                 uv(:,2)=-x(:,3)/R; % z
                uv(nth+nz-1,1)=0; % Correct the pole point u-position.
        end
        %
        % Triangulate in the (u,v)-space using Delaunay triangulation.
        %
        tmp=delaunayTriangulation(uv);
        t=tmp.ConnectivityList;
        nel=size(t,1);
end
%
% Plot the 3D surface
%
ifig=ifig+1; figure(ifig);clf; hold on; 
for i=1:nel
    plot3(x(t(i,[1:3,1]),1),x(t(i,[1:3,1]),2),x(t(i,[1:3,1]),3),'-ok');
end
xlabel('x'); ylabel('y'); zlabel('z')
view(76,15); 
%
% Plot the (u,v)-space
%
figure(100);clf; hold on
triplot(t,uv(:,1),uv(:,2),'-o')
xlabel('u'); ylabel('v')
%%
%
% If the first triangle node of an element is the pole point, shift it so
% that the expansion point is never the pole. 
%
tol=10^-6;
i1=find(abs(uv(t(:,1),2)-1)<tol);
t(i1,[1,2,3])=t(i1,[3,1,2]);

%%
% Use KDTree to find np nearest-neighbors to each point (the WLS stencil)
%
kdtreeNS = KDTreeSearcher(uv);
StencilMap=knnsearch(kdtreeNS,uv,'K',np);
%%
% Set up the surface gradient and integration operators and find the normal
% vectors and elements of surface area for each grid point and element.
%
D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
D(1,1).Wmin=Wmin;

[D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
%
% Add the normal vectors to the plot.
%
figure(ifig); 
fac=1/(3*R);
quiver3(x(:,1), x(:,2), x(:,3), fac*nvec(:,1), fac*nvec(:,2),fac*nvec(:,3),...
    'AutoScale','off'); axis equal; grid on
view(56,30);

%%
% Integrate to find the surface area and volume. 
%
f=ones(N,1); f1=-nvec(:,1).*x(:,1);
f2=-nvec(:,2).*x(:,2); f3=-nvec(:,3).*x(:,3);

S=2*D(1,1).Ivec*f;
V1=2*D(1,1).Ivec*f1;
V2=2*D(1,1).Ivec*f2;
V3=2*D(1,1).Ivec*f3;

Sex=2*pi*R^2;
display(['Relative error in the surface area: ',num2str(abs(S-Sex)/Sex)])

Vex=2/3*pi*R^3;
Vol=(V1+V2)/2;
display(['Relative error in the volume: ',num2str(abs((Vol-Vex)/Vex))])

%% Convergence of the calculations.
%
switch Convergence
    case 'yes'
        %
        % Do a convergence test with this choice of expansion orders and
        % grid spacing. 
        %
        pvec=[2 3 4]; nthvec=[11,21,41];
        R=1;
        Sex=2*pi*R;
        Vex=2/3*pi*R^3;
        
        for io=1:length(pvec)
            p=pvec(io);
            switch p
                case 2
                    sigma=.75;
                case 3
                    sigma=1;
                case 4
                    sigma=1;
                case 5
                    sigma=1.4;
                case 6
                    sigma=1.8;
            end
            % The stencil size in each direction. r=p+3 seems to be the minimum
            % to give a well-conditioned system.
            np=(p+3)^2;    % Total number of points in the stencil
            % Wtype='Unit'; sigma=1;
            Wtype='Gaussian'; Wmin=0.000001;
            
            for in=1:length(nthvec)
                clear('x','uv','t','r','th','ph','D','nvec','ds');

                nth=nthvec(in); nz=(nth+1)/2;
                %
                % The FS boundary points
                %
                dth=pi/(nth-1); dph=pi/2/(nz-1);
                x(1:nth,1)=R*cos(-pi+[0:nth-1]*dth);
                x(1:nth,2)=-R*sin(-pi+[0:nth-1]*dth);
                x(1:nth,3)=0;
                %
                % Then the y=0 boundary points (without duplicates)
                %
                x(nth+1:2*nth-2,1)=R*cos([1:nth-2]*dth);
                x(nth+1:2*nth-2,2)=0;
                x(nth+1:2*nth-2,3)=-sin([1:nth-2]*dth);uiop[]
                 
                %
                % Fill in the rest of the physical points.
                %
                id=length(x); nb=id; % The number of boundary points.
                for j=2:nz-1
                    x(id+1:id+nth-2,1)=R*cos((j-1)*dph)*cos(-pi+[1:nth-2]*dth);
                    x(id+1:id+nth-2,2)=-R*cos((j-1)*dph)*sin(-pi+[1:nth-2]*dth);
                    x(id+1:id+nth-2,3)=-R*sin((j-1)*dph);
                    id=id+nth-2;
                end
                
                N=size(x,1); % The total number of points
                n=N-nb;      % The number of interior points
                %
                % Generate the (u,v)-space as a rectangle. 
                %
                r=sqrt(x(:,1).^2+x(:,2).^2)/R; th=atan2(x(:,2),x(:,1));
                ph=acos(-x(:,3)/R);
                uv(:,1)=-2*(-.5+th./pi); uv(:,2)=2*ph/pi;
                uv(nth+nz-1,1)=0; % Correct the pole point u-position.
                %
                % Triangulate the points with Delaunay triangulation.
                %
                tmp=delaunayTriangulation(uv);
                t=tmp.ConnectivityList;
                nel=size(t,1);
                %
                % If the first triangle node of an element is the pole point, shift it so
                % that the expansion point is never the pole.
                %
                tol=10^-6;
                i1=find(abs(uv(t(:,1),2)-1)<tol);
                t(i1,[1,2,3])=t(i1,[3,1,2]);
                %
                kdtreeNS = KDTreeSearcher(uv);
                StencilMap=knnsearch(kdtreeNS,uv,'K',np);
                %
                %
                D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
                D(1,1).Wmin=Wmin;
                %                
                [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
                
                f=ones(N,1); f1=-nvec(:,1).*x(:,1);
                f2=-nvec(:,2).*x(:,2); 
                
                S=2*D(1,1).Ivec*f;
                V1=2*D(1,1).Ivec*f1;
                V2=2*D(1,1).Ivec*f2;

                errS(in,io)=abs((S-Sex)/Sex);
                Vol=(V1+V2)/2;
                errV(in,io)=abs((Vol-Vex)/Vex);
                
            end
        end
        %
        % Plot the results.
        %%
        ifig=ifig+1; figure(ifig);clf
        loglog(nthvec,errS(:,1),'-+',nthvec,errS(:,2),'-o',nthvec,errS(:,3),'-*'); 
        xlabel('n_\theta'); ylabel('Rel. Error in S');
        b2=polyfit(log(nthvec(2:end)),log(errS(2:end,1)'),1);
        b3=polyfit(log(nthvec(2:end)),log(errS(2:end,2)'),1);
        b4=polyfit(log(nthvec(2:end)),log(errS(2:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],'Location','SouthWest');
        grid on
        
%         PlotToFileColor(gcf,['ScatGridConv_S_',Method,'.eps'],16,12);
        
        ifig=ifig+1; figure(ifig);clf
        loglog(nthvec,errV(:,1),'-+',nthvec,errV(:,2),'-o',nthvec,errV(:,3),'-*')
        xlabel('n_\theta'); ylabel('Rel. Error in V');
        b2=polyfit(log(nthvec(1:end)),log(errV(1:end,1)'),1);
        b3=polyfit(log(nthvec(1:end)),log(errV(1:end,2)'),1);
        b4=polyfit(log(nthvec(1:end)),log(errV(1:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],...
            'Location','SouthWest');
        grid on

%         PlotToFileColor(gcf,['ScatGridConv_V_',Method,'.eps'],16,12);

end