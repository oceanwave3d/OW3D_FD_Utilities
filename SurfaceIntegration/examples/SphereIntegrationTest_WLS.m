%
% This script tests 2D Gaussian and Taylor integration on a sphere defined 
% by a scattered grid of points that have been triangulated into elements. 
% 
% The grid is made in the (u,v)-parameter space using DistMesh, which must
% be in the path. The WLS stencil is formed using GLTree which must also be
% in the path. 
%
% The convergence results are not that clean, compared to a structured 
% grid. For Gauss integration, we get some singular matrix errors which
% could indicate that there are still some bugs in the code. Even so, the
% results are accurate and convergence is roughly order p. For Taylor 
% integration, the integrals are also roughly pth-order. While these 
% results are not as consistent as Taylor integration on a structured grid, 
% accuracy is generally about at the same level for the same number of 
% total elements. Taylor integration is substantially faster than Gauss,
% mainly due to sub-optimal Matlab loops. 
%
% Written by H. Bingham, DTU
% This version: Oct. 5, 2024. 
%
%%
% Include your own path to DistMesh and GLTree here.
%
addpath ~/Dropbox/Work/matlab/distmesh/
addpath ~/Dropbox/Work/matlab/GLTree020109/
%
addpath ../src/
addpath ../QuadTriangle/
%
%%
clear all
ifig=1;
%
% Choose Taylor or Gauss integration
%
% Method='Gauss';
Method='Taylor';
%
% Do a convergence test or not. 
%
% Convergence='no';
Convergence='yes'; 
%
%% One example of expansion order and discretization.
%
% Set up the WLS parameters.
%
p=4;  % Expansion order of the WLS operators
switch p
    case 2
        sigma=1.2;
    case 3
        sigma=1.2;
    case 4
        sigma=1.2;
    case 5
        sigma=1.4;
    case 6
        sigma=1.8;
end
% The stencil size. np=(p+3)^2 seems to be the minimum to give a 
% well-conditioned system.
np=(p+3)^2;    % Total number of points in the stencil
% Wtype='Unit'; sigma=1;
Wtype='Gaussian'; Wmin=0.000001;
%%
% The (u,v) space and the geometry of the sphere.
%
dx=.2;
ifig=1; figure(ifig);clf;
fd=inline('drectangle(p,-1,1,0,1)','p');
[uv,t]=distmesh2d(fd,@huniform,dx,[-1,0;1,1],[-1,0;-1,1;1,0;1,1]);
[uv,t]=fixmesh(uv,t);  % Ensure that the triangles are all ordered counter-clockwise

N=size(uv,1);  % The total number of grid points.
nel=size(t,1); % The total number of triangles.

R=1; 
x=zeros(N,3); 
x(:,1)=R*sin(pi/2*uv(:,2)).*sin(pi/2*uv(:,1)); 
x(:,2)=R*sin(pi/2*uv(:,2)).*cos(pi/2*uv(:,1)); 
x(:,3)=-R*cos(pi/2*uv(:,2));
%
% If the first triangle node of an element is the pole point, shift it so
% that the expansion point is never the pole. 
%
i1=find(uv(t(:,1),2)==1);
t(i1,[1,2,3])=t(i1,[3,1,2]);
%
% Plot the geometry.
%
ifig=ifig+1; figure(ifig);clf; hold on; 
triplot(t,uv(:,1),uv(:,2),'-o')
xlabel('u'); ylabel('v');

ifig=ifig+1; figure(ifig);clf; hold on; 
for i=1:nel
    plot3(x(t(i,[1:3,1]),1),x(t(i,[1:3,1]),2),x(t(i,[1:3,1]),3),'-ok');
end
xlabel('x'); ylabel('y'); zlabel('z')
view(76,15); 

%%
% Use GLTree to find np nearest-neighbors to each point (the WLS stencil)
%
ptrtree=BuildGLTree(uv(:,1),uv(:,2));
StencilMap=KNNSearch(uv(:,1),uv(:,2),uv(:,1),uv(:,2),ptrtree,np);
%%
% Set up the surface gradient and integration operators and find the normal
% vectors and elements of surface area for each grid point and element.
%
D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
D(1,1).Wmin=Wmin;

switch Method
    case 'Gauss'
        [D, nvec]=GaussianIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
    case 'Taylor'
        [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
end
%
% Add the normal vectors to the plot.
%
figure(ifig); 
fac=1/(3*R);
quiver3(x(:,1), x(:,2), x(:,3), fac*nvec(:,1), fac*nvec(:,2),fac*nvec(:,3),...
    'AutoScale','off'); axis equal; grid on
view(56,30);

%%
% Integrate to find the surface area and volume. 
%
f=ones(N,1); f1=-nvec(:,1).*x(:,1);
f2=-nvec(:,2).*x(:,2); f3=-nvec(:,3).*x(:,3);

S=2*D(1,1).Ivec*f;
V1=2*D(1,1).Ivec*f1;
V2=2*D(1,1).Ivec*f2;
V3=2*D(1,1).Ivec*f3;

Sex=2*pi*R^2;
display(['Relative error in the surface area: ',num2str(abs((S-Sex)/Sex))])

Vex=2/3*pi*R^3;
Vol=(V1+V2)/2;
display(['Relative error in the volume: ',num2str(abs((Vol-Vex)/Vex))])


%% Convergence of the calculations.
%
switch Convergence
    case 'yes'
        ifig=ifig+1; figure(ifig); clf;
        %
        % Do a convergence test with this choice of expansion orders and
        % grid spacing. 
        %
        pvec=[2 3 4]; dxvec=[.2 .1 .05];
        R=1;
        Sex=2*pi*R;
        Vex=2/3*pi*R^3;
        
        for io=1:length(pvec)
            p=pvec(io);
            switch p
                case 2
                    sigma=1.2;
                case 3
                    sigma=1.2;
                case 4
                    sigma=1.2;
                case 5
                    sigma=1.4;
                case 6
                    sigma=1.8;
            end
            % The stencil size in each direction. r=p+3 seems to be the minimum
            % to give a well-conditioned system.
            np=(p+3)^2;    % Total number of points in the stencil
            % Wtype='Unit'; sigma=1;
            Wtype='Gaussian'; Wmin=0.000001;
            
            for in=1:length(dxvec)
                dx=dxvec(in);
                fd=inline('drectangle(p,-1,1,0,1)','p');
                clear uv t
                [uv,t]=distmesh2d(fd,@huniform,dx,[-1,0;1,1],[-1,0;-1,1;1,0;1,1]);
                [uv,t]=fixmesh(uv,t);  % Ensure that the triangles are all ordered counter-clockwise
                i1=find(uv(t(:,1),2)==1);
                t(i1,[1,2,3])=t(i1,[3,1,2]);
                
                N=size(uv,1);  % The total number of grid points.
                nel=size(t,1); % The total number of triangles.
                %
                % Define the geometry. Here a 1/2 Wigley hull with the (u,v) origin
                % placed at the stern waterline point, with (u,v) in (-1,1).
                %
                x=zeros(N,3);
                x(:,1)=R*sin(pi/2*uv(:,2)).*sin(pi/2*uv(:,1));
                x(:,2)=R*sin(pi/2*uv(:,2)).*cos(pi/2*uv(:,1));
                x(:,3)=-R*cos(pi/2*uv(:,2));
                %
                % Use GLTree to find np nearest-neighbors to each point (the WLS stencil)
                %
                ptrtree=BuildGLTree(uv(:,1),uv(:,2));
                StencilMap=KNNSearch(uv(:,1),uv(:,2),uv(:,1),uv(:,2),ptrtree,np);
                %
                % Set up the surface gradient and integration operators and find the normal
                % vectors and elements of surface area for each grid point and element.
                %
                clear D
                D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
                D(1,1).Wmin=Wmin; 
                nvec=zeros(N,3); ds=zeros(N,1);
                switch Method
                    case 'Gauss'
                        [D, nvec]=GaussianIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
                    case 'Taylor'
                        [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
                end
                
                f=ones(N,1); f1=-nvec(:,1).*x(:,1);
                f2=-nvec(:,2).*x(:,2); 
                
                S=2*D(1,1).Ivec*f;
                V1=2*D(1,1).Ivec*f1;
                V2=2*D(1,1).Ivec*f2;

                errS(in,io)=abs((S-Sex)/Sex);
                Vol=(V1+V2)/2;
                errV(in,io)=abs((Vol-Vex)/Vex);
                
            end
        end
        %
        % Plot the results.
        %
        ifig=ifig+1; figure(ifig);clf
        loglog(dxvec,errS(:,1),'-+',dxvec,errS(:,2),'-o',dxvec,errS(:,3),'-*'); 
        xlabel('du'); ylabel('Rel. Error in S');
        b2=polyfit(log(dxvec(2:end)),log(errS(2:end,1)'),1);
        b3=polyfit(log(dxvec(2:end)),log(errS(2:end,2)'),1);
        b4=polyfit(log(dxvec(2:end)),log(errS(2:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],'Location','SouthEast');
        grid on
        
%         PlotToFileColor(gcf,['ScatGridConv_S_',Method,'.eps'],16,12);
        
        ifig=ifig+1; figure(ifig);clf
        loglog(dxvec,errV(:,1),'-+',dxvec,errV(:,2),'-o',dxvec,errV(:,3),'-*')
        xlabel('du'); ylabel('Rel. Error in V');
        b2=polyfit(log(dxvec(1:end)),log(errV(1:end,1)'),1);
        b3=polyfit(log(dxvec(1:end)),log(errV(1:end,2)'),1);
        b4=polyfit(log(dxvec(1:end)),log(errV(1:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],...
            'Location','SouthWest');
        grid on

%         PlotToFileColor(gcf,['ScatGridConv_V_',Method,'.eps'],16,12);

 end
