%%
%  This example defines a grid of triangles in the physical space on a 1/2
%  Wigley I or III hull. A simple mapping (u,v)=(2x/L,-1-2z/T) is applied 
%  to define the parameter-space. This gives accurate results for the 
%  surface area and volume calculations. Convergence is order p to p+1. 
%  
%
%  I've generated the grid in three ways: 1. A structured grid, 2. Fixing
%  the boundary points and generating the interior points randomly, 3. 
%  Generate the grid using distmesh, but I throw away the original distmesh
%  u,v space and use the physical grid to recover the u,v space as for the
%  other grids. All three cases give good performance, despite the terrible 
%  grid quality of the 'Random' case. 
%
%  ** It's important here to ensure a roughly equal number of u and v 
%  points in all WLS stencils by scaling the coordinates appropriately when 
%  searching for the nearest neighbors. **
%
%  Either Taylor or Gauss integration can be used. 
%
% Written by H.B. Bingham, DTU, 2022
% This version: Oct. 7, 2024
%
%%
% Include your own path to DistMesh, the integration functions and 
% quadtriangle here.
%
addpath ~/Dropbox/Work/matlab/distmesh/
%
addpath ../src/
addpath ../QuadTriangle/
%
%%
clearvars
ifig=0;
%% Wigley model and physical grid type
%
% Wigley I or III model hull
%
% Wig=1; 
Wig=3; 
%
% Structured, random, distmesh or triangle method for the grid generation.
%
% Grid='structured';
Grid='random';
% Grid='distmesh';
% Grid='triangle';  % nz should be odd for this choice. 
%
% Choose Taylor or Gauss integration
%
% Method='Gauss';
Method='Taylor';

%% Convergence test or not. 
%
Convergence='no';
% Convergence='yes';
%
%% One example of expansion order and discretization.
%
% Set up the WLS parameters.
%
p=4;  % Expansion order of the WLS operators
switch p
    case 2
        sigma=.75;
    case 3
        sigma=1;
    case 4
        sigma=1.2;
    case 5
        sigma=1.4;
    case 6
        sigma=1.8;
end
% The stencil size. np=(p+3)^2 seems to be the minimum to give a 
% well-conditioned system.
np=(p+3)^2;    % Total number of points in the stencil
% Wtype='Unit'; sigma=1;
Wtype='Gaussian'; Wmin=0.000001;
%%
% The scattered point set on the half Wigley hull
%
L=1; B=L/10; T=L/16; 
nzfac=4;
nz=5; nx=nzfac*nz;
%
[x,nvec]=WigleyGrid(nx,nz,Wig,Grid);
N=size(x,1);
%
% Map to (u,v)=(2x/L,1+2z/T)
%
uv(:,1)=2*x(:,1)/L;
uv(:,2)=1+2*x(:,3)/T;
%
% Triangulate the points with Delaunay triangulation.
%
tmp=delaunayTriangulation(uv);
t=tmp.ConnectivityList;
nel=size(t,1);
%
% Plot the 3D surface
%
ifig=ifig+1; figure(ifig);clf; hold on; 
for i=1:nel
    plot3(x(t(i,[1:3,1]),1),x(t(i,[1:3,1]),2),x(t(i,[1:3,1]),3),'-ok');
end
xlabel('x'); ylabel('y'); zlabel('z')
view(76,15); 
%
% Plot the (u-v) space
%
figure(100);clf; 
triplot(t,uv(:,1),uv(:,2),'-o')
xlabel('u'); ylabel('v')

%%
% Use KDTree to find np nearest-neighbors to each point (the WLS stencil)
%
% To ensure a roughly even number of points in each direction, scale the
% (u,v)-space accordingly for the neighbor search. 
%
uvs=[uv(:,1),uv(:,2)/nzfac];
%
kdtreeNS = KDTreeSearcher(uvs);
[StencilMap,dist]=knnsearch(kdtreeNS,uvs,'K',np);
%%
% Set up the surface gradient and integration operators and find the normal
% vectors and elements of surface area for each grid point and element.
%
D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
D(1,1).Wmin=Wmin;

switch Method
    case 'Taylor'
        [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
    case 'Gauss'
        [D, nvec]=GaussianIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
end
%
% Add the normal vectors to the plot.
%
figure(ifig); 
fac=1/(30*L);
quiver3(x(:,1), x(:,2), x(:,3), fac*nvec(:,1), fac*nvec(:,2),fac*nvec(:,3),...
    'AutoScale','off'); axis equal; grid on
view(100,-16);

%%
% Integrate to find the surface area and volume. 
%
f=ones(N,1); f1=-nvec(:,1).*x(:,1);
f2=-nvec(:,2).*x(:,2); f3=-nvec(:,3).*x(:,3);

switch Wig
    case 1
        Sex=0.16118388498047598;
        Vex=3643/1039500;
    case 3
        Sex=0.15013441092291333;
        Vex=13/4500;
end

S=2*D(1,1).Ivec*f;
V1=2*D(1,1).Ivec*f1;
V2=2*D(1,1).Ivec*f2;
V3=2*D(1,1).Ivec*f3;


display(['Relative error in the surface area: ',num2str(abs(S-Sex)/Sex)])

Vol=(V1+V2)/2;

display(['Relative error in the volume: ',num2str(abs((Vol-Vex)/Vex))])

%% Convergence of the calculations.
%
switch Convergence
    case 'yes'
        %
        % Do a convergence test with this choice of expansion orders and
        % grid spacing. 
        %
        pvec=[2 3 4]; nzvec=[5,9,17,33]; nxvec=nzfac*nzvec;
        L=1; B=L/10; T=L/16;
        switch Wig
            case 1
                Sex=0.16118388498047598;
                Vex=3643/1039500;
            case 3
                Sex=0.15013441092291333;
                Vex=13/4500;
        end
        
        for io=1:length(pvec)
            p=pvec(io);
            switch p
                case 2
                    sigma=.75;
                case 3
                    sigma=1;
                case 4
                    sigma=1;
                case 5
                    sigma=1.4;
                case 6
                    sigma=1.8;
            end
            % The stencil size in each direction. r=p+3 seems to be the minimum
            % to give a well-conditioned system.
            np=(p+3)^2;    % Total number of points in the stencil
            % Wtype='Unit'; sigma=1;
            Wtype='Gaussian'; Wmin=0.000001;
            
            for in=1:length(nxvec)
                clear('x','uv','t','r','th','ph','D','nvec','ds');

                nx=nxvec(in); nz=nzvec(in);
                %
                % The hull points
                %
                [x,nvec]=WigleyGrid(nx,nz,Wig,Grid);
                N=size(x,1);
                %
                % Map to (u,v)=(x/L,z/T)
                %
                uv(:,1)=2*x(:,1)/L;
                uv(:,2)=-1+2*x(:,3)/T;
                %
                % Triangulate the points with Delaunay triangulation.
                %
                tmp=delaunayTriangulation(uv);
                t=tmp.ConnectivityList;
                nel=size(t,1);
                %
                % Find the stencils
                %
                uvs=[uv(:,1),uv(:,2)/nzfac];
                %
                kdtreeNS = KDTreeSearcher(uvs);
                StencilMap=knnsearch(kdtreeNS,uvs,'K',np);
                %
                D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
                D(1,1).Wmin=Wmin;
                %                
                switch Method
                    case 'Taylor'
                        [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
                    case 'Gauss'
                        [D, nvec]=GaussianIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
                end
                
                f=ones(N,1); f1=-nvec(:,1).*x(:,1);
                f2=-nvec(:,2).*x(:,2); 
                
                S=2*D(1,1).Ivec*f;
                V1=2*D(1,1).Ivec*f1;
                V2=2*D(1,1).Ivec*f2;

                errS(in,io)=abs((S-Sex)/Sex);
                Vol=(V1+V2)/2;
                errV(in,io)=abs((Vol-Vex)/Vex);
                
            end
        end
        %%
        % Plot the results.
        %
        ifig=ifig+1; figure(ifig);clf
        loglog(nxvec,errS(:,1),'-+',nxvec,errS(:,2),'-o',nxvec,errS(:,3),'-*'); 
        xlabel('n_x'); ylabel('Rel. Error in S');
        b2=polyfit(log(nxvec(2:end)),log(errS(2:end,1)'),1);
        b3=polyfit(log(nxvec(2:end)),log(errS(2:end,2)'),1);
        b4=polyfit(log(nxvec(2:end)),log(errS(2:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],'Location','SouthWest');
        grid on
        
        
        ifig=ifig+1; figure(ifig);clf
        loglog(nxvec,errV(:,1),'-+',nxvec,errV(:,2),'-o',nxvec,errV(:,3),'-*')
        xlabel('n_x'); ylabel('Rel. Error in V');
        b2=polyfit(log(nxvec(1:end)),log(errV(1:end,1)'),1);
        b3=polyfit(log(nxvec(1:end)),log(errV(1:end,2)'),1);
        b4=polyfit(log(nxvec(1:end)),log(errV(1:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],...
            'Location','SouthWest');
        grid on
        %
        % Save the data for post-processing.
        %
        save(['WigleyScatGridPhysConv_',Grid,'_',Method,'_Wig',Wig,'.mat'],...
            'Grid','Method','Wig','nxvec','errS','errV');
end

