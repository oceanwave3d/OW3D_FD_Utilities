%%
%  This example defines a grid of triangles in the physical space on a 1/2
%  Barge. The physical points are projected onto the plane by normalizing
%  and stretching their positions based on the y-coordinate. Despite the
%  singularies at the corners, the normal vectors are mostly relatively
%  accurate, as are the volume and the surface area. 
%
%  Things get worse at the corners as the order increases. Convergence is 
%  erratic and not better than first order. 
%  

%  Either Taylor or Gauss integration can be used. 
%
% Written by H.B. Bingham, DTU, 2022
% This version: March 2, 2024
%
%%
addpath ../src/
%
%%
clearvars
ifig=0;
%% Integration method
% Choose Taylor or Gauss integration
%
Method='Gauss';
% Method='Taylor';

%% Convergence test or not. 

% Convergence='no';
Convergence='yes';
%
%% One example of expansion order and discretization.
%
% Set up the WLS parameters.
%
p=2;  % Expansion order of the WLS operators
switch p
    case 2
        sigma=.75;
    case 3
        sigma=1;
    case 4
        sigma=1.2;
    case 5
        sigma=1.4;
    case 6
        sigma=1.8;
end
% The stencil size. np=(p+3)^2 seems to be the minimum to give a 
% well-conditioned system.
np=(p+3)^2;    % Total number of points in the stencil
% Wtype='Unit'; sigma=1;
Wtype='Gaussian'; Wmin=0.000001;
%%
% The scattered point set on the half barge
%
L=1; B=L/10; T=L/16; 
nx=11; ny=(nx+1)/2; nz=(nx+1)/2; 
%
% The hull points 
%
dx=L/(nx-1); dy=B/(ny-1)/2; dz=T/(nz-1); nw=nx+2*(ny-1);
% The waterline points
x(1:ny,1)=-L/2; x(1:ny,2)=[0:ny-1]'*dy; x(1:ny,3)=0;
x(ny+1:ny+nx-1,1)=-L/2+[1:nx-1]'*dx; x(ny+1:ny+nx-1,2)=B/2; x(ny+1:ny+nx-1,3)=0;
x(ny+nx:nw,1)=L/2; x(ny+nx:nw,2)=B/2-[1:ny-1]'*dy; x(ny+nx:nw,3)=0;
% The front, back and side points
for j=2:nz
    x((j-1)*nw+1:j*nw,1)=x(1:nw,1); x((j-1)*nw+1:j*nw,2)=x(1:nw,2);
    x((j-1)*nw+1:j*nw,3)=-(j-1)*dz;
end
% The bottom points
id=nw*nz;
for j=1:ny-1
    x(id+(j-1)*(nx-2)+1:id+j*(nx-2),1)=x(ny+1:ny+nx-2,1);
    x(id+(j-1)*(nx-2)+1:id+j*(nx-2),2)=x(j,2);
    x(id+(j-1)*(nx-2)+1:id+j*(nx-2),3)=-T;
end
N=id+(nx-2)*(ny-1);
%
% Map to angles in the two planes. I can't get this to work. 
%
% th=atan2(2*(.25-x(:,2)/B),x(:,1)/L);
% ph=atan2((.5+x(:,3)/T),2*(.25+x(:,2)/B));
% uv(:,1)=-1+2*th./pi; %uv(:,2)=-1+2*ph/pi;  
% uv(:,2)=2*ph./pi;
%
% Collapse the points onto the plane by stretching based on the y-position.
fac1=1;
uv(:,1)=2*x(:,1)/L.*(1-fac1.*x(:,2)/B);         % -1:1 in u
uv(:,2)=1+(x(:,3)/T).*(1-fac1.*x(:,2)/B);       %  0:1 in v
%
% Triangulate the points with Delaunay triangulation. 
%
tmp=delaunayTriangulation(uv);
t=tmp.ConnectivityList;
nel=size(t,1);
%
% Plot the 3D surface
%
ifig=ifig+1; figure(ifig);clf; hold on; 
for i=1:nel
    plot3(x(t(i,[1:3,1]),1),x(t(i,[1:3,1]),2),x(t(i,[1:3,1]),3),'-ok');
end
xlabel('x'); ylabel('y'); zlabel('z')
view(76,15); 
%
% Plot the (u-v) space
%
figure(100);clf; 
triplot(t,uv(:,1),uv(:,2),'-o')
xlabel('u'); ylabel('v')


%%
% Use KDTree to find np nearest-neighbors to each point (the WLS stencil)
%
% To ensure a roughly even number of points in each direction, scale the
% (u,v)-space accordingly for the neighbor search. 
%
Lu=max(uv(:,1))-min(uv(:,1)); Lv=max(uv(:,2))-min(uv(:,2));
fac=nw/ny * Lv/Lu;
uvs=[uv(:,1),uv(:,2)/fac];
%
kdtreeNS = KDTreeSearcher(uvs);
[StencilMap,dist]=knnsearch(kdtreeNS,uvs,'K',np);
%%
% Set up the surface gradient and integration operators and find the normal
% vectors and elements of surface area for each grid point and element.
%
D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
D(1,1).Wmin=Wmin;

switch Method
    case 'Taylor'
        [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
    case 'Gauss'
        [D, nvec]=GaussianIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
end
%
% Add the normal vectors to the plot.
%
figure(ifig); 
fac=1/(30*L);
quiver3(x(:,1), x(:,2), x(:,3), fac*nvec(:,1), fac*nvec(:,2),fac*nvec(:,3),...
    'AutoScale','off'); axis equal; grid on
view(100,-16);

%%
% Integrate to find the surface area and volume. 
%
f=ones(N,1); f1=-nvec(:,1).*x(:,1);
f2=-nvec(:,2).*x(:,2); f3=-nvec(:,3).*x(:,3);

Sex=L*(2*T+B)+2*B*T;
Vex=L*B*T;

S=2*D(1,1).Ivec*f;
V1=2*D(1,1).Ivec*f1;
V2=2*D(1,1).Ivec*f2;
V3=2*D(1,1).Ivec*f3;


display(['Relative error in the surface area: ',num2str(abs(S-Sex)/Sex)])

Vol=(V1+V2)/2;

display(['Relative error in the volume: ',num2str(abs((Vol-Vex)/Vex))])

%%
% Convergence test
%
switch Convergence
    case 'yes'
        nxvec=[11,21,41]; pvec=[2,3,4];
        for io=1:length(pvec)
            p=pvec(io);
            switch p
                case 2
                    sigma=.75;
                case 3
                    sigma=1;
                case 4
                    sigma=1;
                case 5
                    sigma=1.4;
                case 6
                    sigma=1.8;
            end
            % The stencil size in each direction. r=p+3 seems to be the minimum
            % to give a well-conditioned system.
            np=(p+3)^2;    % Total number of points in the stencil
            % Wtype='Unit'; sigma=1;
            Wtype='Gaussian'; Wmin=0.000001;

            for in=1:length(nxvec)
                clear('x','uv','t','r','th','ph','D','nvec','ds');
                nx=nxvec(in); ny=(nx+1)/2; nz=(nx+1)/2;
                % The hull points
                dx=L/(nx-1); dy=B/(ny-1)/2; dz=T/(nz-1); nw=nx+2*(ny-1);
                % The waterline points
                x(1:ny,1)=-L/2; x(1:ny,2)=[0:ny-1]'*dy; x(1:ny,3)=0;
                x(ny+1:ny+nx-1,1)=-L/2+[1:nx-1]'*dx; x(ny+1:ny+nx-1,2)=B/2; x(ny+1:ny+nx-1,3)=0;
                x(ny+nx:nw,1)=L/2; x(ny+nx:nw,2)=B/2-[1:ny-1]'*dy; x(ny+nx:nw,3)=0;
                % The front, back and side points
                for j=2:nz
                    x((j-1)*nw+1:j*nw,1)=x(1:nw,1); x((j-1)*nw+1:j*nw,2)=x(1:nw,2);
                    x((j-1)*nw+1:j*nw,3)=-(j-1)*dz;
                end
                % The bottom points
                id=nw*nz;
                for j=1:ny-1
                    x(id+(j-1)*(nx-2)+1:id+j*(nx-2),1)=x(ny+1:ny+nx-2,1);
                    x(id+(j-1)*(nx-2)+1:id+j*(nx-2),2)=x(j,2);
                    x(id+(j-1)*(nx-2)+1:id+j*(nx-2),3)=-T;
                end
                N=id+(nx-2)*(ny-1);
                % Collapse the points onto the plane by stretching based on the y-position.
                fac1=1;
                uv(:,1)=2*x(:,1)/L.*(1-fac1.*x(:,2)/B);         % -1:1 in u
                uv(:,2)=1+(x(:,3)/T).*(1-fac1.*x(:,2)/B);       %  0:1 in v
                % Triangulate the points with Delaunay triangulation.
                tmp=delaunayTriangulation(uv);
                t=tmp.ConnectivityList;
                nel=size(t,1);
                % Use KDTree to find np nearest-neighbors to each point (the WLS stencil)
                Lu=max(uv(:,1))-min(uv(:,1)); Lv=max(uv(:,2))-min(uv(:,2));
                fac=nw/ny * Lv/Lu;
                uvs=[uv(:,1),uv(:,2)/fac];
                %
                kdtreeNS = KDTreeSearcher(uvs);
                [StencilMap,dist]=knnsearch(kdtreeNS,uvs,'K',np);
                % Set up the surface gradient and integration operators and find the normal
                % vectors and elements of surface area for each grid point and element.
                D(1,1).p=p; D(1,1).np=np; D(1,1).Wtype=Wtype; D(1,1).sigma=sigma;
                D(1,1).Wmin=Wmin;
                switch Method
                    case 'Taylor'
                        [D, nvec]=TaylorIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
                    case 'Gauss'
                        [D, nvec]=GaussianIntegration2D_WLS_SetUp(x,uv,StencilMap,t,D);
                end
                % Integrate to find the surface area and volume.
                %
                f=ones(N,1); f1=-nvec(:,1).*x(:,1);
                f2=-nvec(:,2).*x(:,2); f3=-nvec(:,3).*x(:,3);
                S=2*D(1,1).Ivec*f;
                V1=2*D(1,1).Ivec*f1;
                V2=2*D(1,1).Ivec*f2;
                V3=2*D(1,1).Ivec*f3;
                Vol=(V1+V2+V3)/3;
                errS(in,io)=abs((S-Sex)/Sex);
                errV(in,io)=abs((Vol-Vex)/Vex);

            end
        end
        %
        % Plot the results.
        %
        ifig=ifig+1; figure(ifig);clf
        loglog(nxvec,errS(:,1),'-+',nxvec,errS(:,2),'-o',nxvec,errS(:,3),'-*'); 
        xlabel('n_x'); ylabel('Rel. Error in S');
        b2=polyfit(log(nxvec(2:end)),log(errS(2:end,1)'),1);
        b3=polyfit(log(nxvec(2:end)),log(errS(2:end,2)'),1);
        b4=polyfit(log(nxvec(2:end)),log(errS(2:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],'Location','SouthWest');
        grid on
        
        
        ifig=ifig+1; figure(ifig);clf
        loglog(nxvec,errV(:,1),'-+',nxvec,errV(:,2),'-o',nxvec,errV(:,3),'-*')
        xlabel('n_x'); ylabel('Rel. Error in V');
        b2=polyfit(log(nxvec(1:end)),log(errV(1:end,1)'),1);
        b3=polyfit(log(nxvec(1:end)),log(errV(1:end,2)'),1);
        b4=polyfit(log(nxvec(1:end)),log(errV(1:end,3)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=3, s=',num2str(b3(1))],...
            ['p=4, s=',num2str(b4(1))],...
            'Location','SouthWest');
        grid on
        %
        % Save the data for post-processing.
        %
        save(['BargeGridPhysConv_',Method,'.mat'],...
            'Method','nxvec','errS','errV');

        
end

