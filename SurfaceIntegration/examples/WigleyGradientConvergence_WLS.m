%
% This script generates a structured or unstructured grid on the Wigley 
% hull, applies a deep water incident wave potential to each point and 
% takes the first- and second-gradients using a WLS method following the 
% method of Bingham and Maniar IWWWFB (1996). Note that there are a 
% couple of sign  errors in the equations from that paper, which are 
% noted in the function 'ComputeMtermsWLS.m' which must be in the path. 
%
% For an order-p WLS stencil, the gradient converges with order p
% and the second gradients with order p-1, both for a structured,  
% distmesh or a triangular grid, though the max. errors are much larger on 
% the triangle grid. 
%
% The grid is generated using the function 'WigleyGrid.m' which needs to 
% be in the path. 
%
% Written by H.B. Bingham, Oct. 2, 2024.
%
% This version: Oct. 8, 2024.
%
%%
%
clear all
addpath ../src
%
% Include your own path to DistMesh if you want to use it. 
%
addpath ~/Dropbox/Work/matlab/distmesh/
ifig=0;
%%
%
% Choose to either show convergence for all derivatives or to make plots 
% for a single discretization. 
%
% job='single'
job='convergence'
%
% Choose the Wigley model I or III.
%
Wig=1;
% Wig=3;
L=1; B=L/10; T=L/16;
%
% Choose the grid type: 'structured', 'distmesh' or 'triangle'.
%
%Grid='structured';
Grid='distmesh';
% Grid='triangle';
%
% Choose the ratio of nx/nz. 
%
nzfac=4;
%
% Choose the deep water incident wave to force with
%
k=4*pi;    % \lambda=L/2;
beta=pi/6; cb=cos(beta); sb=sin(beta);
%
%%
switch job
    case 'convergence'
        % 
        % The discretizations to consider.
        %
        nvvec=[9,17,33,65]; nuvec=nzfac*nvvec; 
        %
        % The WLS order 
        %
        p=4; 
        %
        for i=1:length(nvvec)
            clear uv
            nu=nuvec(i); nv=nvvec(i); np=nu*nv;
            %
            nx=nu; nz=nv;
            %
            % The hull points
            %
            [x,nvec]=WigleyGrid(nx,nz,Wig,Grid);
            %
            % Map to (u,v)=(2x/L,1+2z/T)
            %
            uv(:,1)=1+2*x(:,1)/L;
            uv(:,2)=1+2*x(:,3)/T;
            %%
            % Plug in an exact wave solution for phi
            %
            theta=k*(cb*x(:,1)+sb*x(:,2));
            phi=exp(k*x(:,3)).*cos(theta);
            phix=-k*cb*exp(k*x(:,3)).*sin(theta);
            phiy=-k*sb*exp(k*x(:,3)).*sin(theta);
            phiz=k*phi;
            phin=(nvec(:,1).*phix + nvec(:,2).*phiy + nvec(:,3).*phiz); 
            
            phixx=-k^2*cb^2*exp(k*x(:,3)).*cos(theta);
            phixy=-k^2*cb*sb*exp(k*x(:,3)).*cos(theta);
            phixz=-k^2*cb*exp(k*x(:,3)).*sin(theta);
            phiyy=-k^2*sb^2*exp(k*x(:,3)).*cos(theta);
            phiyz=-k^2*sb*exp(k*x(:,3)).*sin(theta);
            phizz=k^2*phi;            
            %
            % Take all of the derivatives 
            %  
            uv=[uv(:,1),uv(:,2)/nzfac];
            [n,GradPhi,Grad2Phi,m]=ComputeMtermsWLS(x,uv,p,phi,phin);
            
            GradErr(i,1)=max(abs(phix-GradPhi(:,1)))/max(abs(phix));
            GradErr(i,2)=max(abs(phiy-GradPhi(:,2)))/max(abs(phiy));
            GradErr(i,3)=max(abs(phiz-GradPhi(:,3)))/max(abs(phiz));
            Grad2Err(i,1)=max(abs(phixx-Grad2Phi(:,1)))/max(abs(phixx));
            Grad2Err(i,2)=max(abs(phixy-Grad2Phi(:,2)))/max(abs(phixy));
            Grad2Err(i,3)=max(abs(phixz-Grad2Phi(:,3)))/max(abs(phixz));
            Grad2Err(i,4)=max(abs(phiyy-Grad2Phi(:,4)))/max(abs(phiyy));
            Grad2Err(i,5)=max(abs(phiyz-Grad2Phi(:,5)))/max(abs(phiyz));
            Grad2Err(i,6)=max(abs(phizz-Grad2Phi(:,6)))/max(abs(phizz));
            
            GradErrA(i,1)=mean(abs(phix-GradPhi(:,1)))/max(abs(phix));
            GradErrA(i,2)=mean(abs(phiy-GradPhi(:,2)))/max(abs(phiy));
            GradErrA(i,3)=mean(abs(phiz-GradPhi(:,3)))/max(abs(phiz));
            Grad2ErrA(i,1)=mean(abs(phixx-Grad2Phi(:,1)))/max(abs(phixx));
            Grad2ErrA(i,2)=mean(abs(phixy-Grad2Phi(:,2)))/max(abs(phixy));
            Grad2ErrA(i,3)=mean(abs(phixz-Grad2Phi(:,3)))/max(abs(phixz));
            Grad2ErrA(i,4)=mean(abs(phiyy-Grad2Phi(:,4)))/max(abs(phiyy));
            Grad2ErrA(i,5)=mean(abs(phiyz-Grad2Phi(:,5)))/max(abs(phiyz));
            Grad2ErrA(i,6)=mean(abs(phizz-Grad2Phi(:,6)))/max(abs(phizz));
            
        end
        %%
        bu=polyfit(log(nuvec(end-2:end)),log(GradErr(end-2:end,1)),1);
        bux=polyfit(log(nuvec(end-2:end)),log(Grad2Err(end-2:end,1)),1);
        buA=polyfit(log(nuvec(end-2:end)),log(GradErrA(end-2:end,1)),1);
        buxA=polyfit(log(nuvec(end-2:end)),log(Grad2ErrA(end-2:end,1)),1);
        
        %
        ifig=ifig+1; figure(ifig); clf
        loglog(nuvec,GradErr)
        grid on; xlabel('nx'); ylabel('Max error'); 
        legend('u','v','w')
        text(50,10^-4,['Slope of u-error=',num2str(bu(1))])
        title(['Convergence of order ',num2str(p),' WLS schemes on a ',Grid,' grid']);
        ifig=ifig+1; figure(ifig); clf
        loglog(nuvec,GradErrA)
        grid on; xlabel('nx'); ylabel('Mean error'); 
        legend('u','v','w')
        text(50,10^-5,['Slope of u-error=',num2str(buA(1))])
        title(['Convergence of order ',num2str(p),' WLS schemes on a ',Grid,' grid']);

        ifig=ifig+1; figure(ifig); clf
        loglog(nuvec,Grad2Err)
        grid on; xlabel('nx'); ylabel('Max error'); 
        legend('u_x','u_y','u_z','v_y','v_z','w_z')
        text(50,10^-2,['Slope of u_x-error=',num2str(bux(1))])
        title(['Convergence of order ',num2str(p),' WLS schemes on a ',Grid,' grid']);

        ifig=ifig+1; figure(ifig); clf
        loglog(nuvec,Grad2ErrA)
        grid on; xlabel('nx'); ylabel('Mean error'); 
        legend('u_x','u_y','u_z','v_y','v_z','w_z')
        text(50,10^-4,['Slope of u_x-error=',num2str(buxA(1))])
        title(['Convergence of order ',num2str(p),' WLS schemes on a ',Grid,' grid']);

    case 'single'
        %% Test one discretization and WLS order. 
        %
        % WLS order
        %
        p=4;
        %
        % The discretzation, (nz should be odd for a 'triangle' grid). 
        %
        nz=9; nx=nzfac*nz;
        %
        % The hull points
        %
        [x,nvec]=WigleyGrid(nx,nz,Wig,Grid);
        N=size(x,1);
        %
        % Map to (u,v)=(2x/L,1+2z/T)
        %
        uv(:,1)=2*x(:,1)/L;
        uv(:,2)=1+2*x(:,3)/T;
        %
        ifig=ifig+1; figure(ifig); clf; hold on;
        plot3(x(:,1),x(:,2),x(:,3),'o')
        quiver3(x(:,1), x(:,2), x(:,3), nvec(:,1), nvec(:,2),nvec(:,3), 'MarkerSize', 4); 
        
        AZ=40; EL=20; view(AZ,EL); xlabel('x'); ylabel('y'); zlabel('z'); axis('equal'); hold off;
        %%
        % The exact solution.
        %
        theta=k*(cb*x(:,1)+sb*x(:,2));
        phi=exp(k*x(:,3)).*cos(theta);
        phix=-k*cb*exp(k*x(:,3)).*sin(theta);
        phiy=-k*sb*exp(k*x(:,3)).*sin(theta);
        phiz=k*phi;
        phin=(nvec(:,1).*phix + nvec(:,2).*phiy + nvec(:,3).*phiz); 
        phixx=-k^2*cb^2*exp(k*x(:,3)).*cos(theta);
        phixy=-k^2*cb*sb*exp(k*x(:,3)).*cos(theta);
        phixz=-k^2*cb*exp(k*x(:,3)).*sin(theta);
        phiyy=-k^2*sb^2*exp(k*x(:,3)).*cos(theta);
        phiyz=-k^2*sb*exp(k*x(:,3)).*sin(theta);
        phizz=k^2*phi;        
        %
        % Take all of the derivatives of the potential using an order p
        % WLS scheme. This is implemented in the function ComputeMTermsWLS.m.
        %
        % To ensure a roughly even number of points in each direction, scale the
        % (u,v)-space accordingly for the nearest-neighbor search.
        %
        uv=[uv(:,1),uv(:,2)/nzfac];
        %
        [n,GradPhi,Grad2Phi,m]=ComputeMtermsWLS(x,uv,p,phi,phin);
        
        ifig=ifig+1; figure(ifig); clf; hold on
        plot3(x(:,1),x(:,2),x(:,3),'o')
        quiver3(x(:,1), x(:,2), x(:,3), n(:,1), n(:,2),n(:,3), 'MarkerSize', 4); axis equal;
        AZ=40; EL=20; view(AZ,EL); xlabel('x'); ylabel('y'); zlabel('z'); axis('equal'); hold off;
       
        errx=(phix-GradPhi(:,1))/max(abs(phix));
        erry=(phiy-GradPhi(:,2))/max(abs(phiy));
        errz=(phiz-GradPhi(:,3))/max(abs(phiz));
        errxx=(phixx-Grad2Phi(:,1))/max(abs(phixx));
        errxy=(phixy-Grad2Phi(:,2))/max(abs(phixy));
        errxz=(phixz-Grad2Phi(:,3))/max(abs(phixz));
        erryy=(phiyy-Grad2Phi(:,4))/max(abs(phiyy));
        erryz=(phiyz-Grad2Phi(:,5))/max(abs(phiyz));
        errzz=(phizz-Grad2Phi(:,6))/max(abs(phizz));
        %
        %% Plot up all of the errors
        %
        ifig=ifig+1; figure(ifig); clf;
        % plot(x(:,1),phix,'o',x(:,1),GradPhi(:,1),'+');
        plot(x(:,1),errx,'o');
        xlabel('x'); ylabel('Error in u');
        grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        % plot(x(:,1),phiy,'o',x(:,1),GradPhi(:,2),'+');
        plot(x(:,1),erry,'o');
        xlabel('x'); ylabel('Error in v');
        grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        % plot(x(:,1),phiz,'o',x(:,1),GradPhi(:,3),'+');
        plot(x(:,1),errz,'o');
        xlabel('x'); ylabel('Error in w');
        grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(x(:,1),errxx,'o'); xlabel('x');
        ylabel('Error in u_x'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(x(:,1),erryy,'o'); xlabel('x');
        ylabel('Error in v_y'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(x(:,1),errzz,'o'); xlabel('x');
        ylabel('Error in w_z'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(x(:,1),errxy,'o'); xlabel('x');
        ylabel('Error in u_y'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(x(:,1),errxz,'o'); xlabel('x');
        ylabel('Error in u_z'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(x(:,1),erryz,'o'); xlabel('x');
        ylabel('Error in v_z'); grid on;
        %
        % Check the Laplacian which should be zero.
        %
        contFD=Grad2Phi(:,1)+Grad2Phi(:,4)+Grad2Phi(:,6);
        ifig=ifig+1; figure(ifig); clf;
        plot(x(:,1),contFD,'o'); xlabel('x'); ylabel('div(u)'); grid on
        
end

