%
% This script generates a structured grid on the Wigley hull, applied a
% deep water incident wave potential to each point and takes the first-
% and second-gradients using a high-order FD method following the method of 
% Bingham and Maniar IWWWFB (1996). Note that there are a couple of sign 
% errors in the equations from that paper, which are noted in the function 
% 'ComputeMTermsFD.m' which must be in the path. The grid is generated by
% the function 'WigleyGrid.m' which must also be in the path. 
%
% For a (2 alpha+1) FD stencil, the gradient converges with order 2 alpha
% and the second gradients with order 2 alpha-1, as expected.  
%
% Written by H.B. Bingham, Sept. 29, 2024.
% This version: Oct. 7, 2024
%
%%
%
clear all
addpath ../src
ifig=0;
%
% Choose to either show convergence for all derivatives or to make plots of
% them for a single discretization. 
%
% job='single'
job='convergence'
%%
switch job
    case 'convergence'
        % The discretizations to consider.
        nvvec=[8,12,16,32,64]; nuvec=4*nvvec; 
        alpha=2; % The FD stencil is (2 alpha +1)
        Wig=3;  % Wigley model type
        Grid='structured'; % Grid type

        % The incident wave to force with
        k=4*pi;    % \lambda=L/2;
        beta=pi/6; cb=cos(beta); sb=sin(beta);

        for i=1:length(nvvec)
            nu=nuvec(i); nv=nvvec(i); np=nu*nv;
            [xc,nvec]=WigleyGrid(nu,nv,Wig,Grid);
            %%
            % Plug in an exact wave solution for phi
            %
            theta=k*(cb*xc(:,1)+sb*xc(:,2));            
            phi=exp(k*xc(:,3)).*cos(theta);
            phix=-k*cb*exp(k*xc(:,3)).*sin(theta);
            phiy=-k*sb*exp(k*xc(:,3)).*sin(theta);
            phiz=k*phi;
            phin=(nvec(:,1).*phix + nvec(:,2).*phiy + nvec(:,3).*phiz); 
            
            phixx=-k^2*cb^2*exp(k*xc(:,3)).*cos(theta);
            phixy=-k^2*cb*sb*exp(k*xc(:,3)).*cos(theta);
            phixz=-k^2*cb*exp(k*xc(:,3)).*sin(theta);
            phiyy=-k^2*sb^2*exp(k*xc(:,3)).*cos(theta);
            phiyz=-k^2*sb*exp(k*xc(:,3)).*sin(theta);
            phizz=k^2*phi;            
            %
            % Take all of the derivatives 
            %
            [n,GradPhi,Grad2Phi,m]=ComputeMtermsFD(phi,xc,alpha,nu,nv,phin);
            
            GradErr(i,1)=max(abs(phix-GradPhi(:,1)))/max(abs(phix));
            GradErr(i,2)=max(abs(phiy-GradPhi(:,2)))/max(abs(phiy));
            GradErr(i,3)=max(abs(phiz-GradPhi(:,3)))/max(abs(phiz));
            Grad2Err(i,1)=max(abs(phixx-Grad2Phi(:,1)))/max(abs(phixx));
            Grad2Err(i,2)=max(abs(phixy-Grad2Phi(:,2)))/max(abs(phixy));
            Grad2Err(i,3)=max(abs(phixz-Grad2Phi(:,3)))/max(abs(phixz));
            Grad2Err(i,4)=max(abs(phiyy-Grad2Phi(:,4)))/max(abs(phiyy));
            Grad2Err(i,5)=max(abs(phiyz-Grad2Phi(:,5)))/max(abs(phiyz));
            Grad2Err(i,6)=max(abs(phizz-Grad2Phi(:,6)))/max(abs(phizz));
        end
        bu=polyfit(log(nuvec(end-2:end)),log(GradErr(end-2:end,1)),1);
        bux=polyfit(log(nuvec(end-2:end)),log(Grad2Err(end-2:end,1)),1);
        
        %
        ifig=ifig+1; figure(ifig); clf
        loglog(nuvec,GradErr)
        grid on; xlabel('nx'); ylabel('Max error'); 
        legend('u','v','w')
        text(50,10^-6,['Slope of u-error=',num2str(bu(1))])
        title(['Convergence of order ',num2str(2*alpha),' FD schemes']);
 
        ifig=ifig+1; figure(ifig); clf
        loglog(nuvec,Grad2Err)
        grid on; xlabel('nx'); ylabel('Max error'); 
        legend('u_x','u_y','u_z','v_y','v_z','w_z')
        text(50,10^-5,['Slope of u_x-error=',num2str(bux(1))])
        title(['Convergence of order ',num2str(2*alpha),' FD schemes']);
 
    case 'single'
        %% Generate the Wigley hull on a structured grid
        %
        Wig=3;  % Wigley model type
        Grid='structured'; % Grid type

        nu=24; nv=8; np=nu*nv;
        
        [xc,nvec]=WigleyGrid(nu,nv,Wig,Grid);
        %
        ifig=ifig+1; figure(ifig); clf; hold on;
        plot3(xc(:,1),xc(:,2),xc(:,3),'o')
        quiver3(xc(:,1), xc(:,2), xc(:,3), nvec(:,1), nvec(:,2),nvec(:,3), 'MarkerSize', 4); 
        
        AZ=40; EL=20; view(AZ,EL); xlabel('x'); ylabel('y'); zlabel('z'); axis('equal'); hold off;
        
        %%
        % Plug in an exact wave solution for phi and compute the exact
        % derivatives.
        %
        k=4*pi;    % \lambda=L/2;
        beta=pi/6; cb=cos(beta); sb=sin(beta);
        theta=k*(cb*xc(:,1)+sb*xc(:,2));
        
        phi=exp(k*xc(:,3)).*cos(theta);
        phix=-k*cb*exp(k*xc(:,3)).*sin(theta);
        phiy=-k*sb*exp(k*xc(:,3)).*sin(theta);
        phiz=k*phi;
        phin=(nvec(:,1).*phix + nvec(:,2).*phiy + nvec(:,3).*phiz); 
        
        phixx=-k^2*cb^2*exp(k*xc(:,3)).*cos(theta);
        phixy=-k^2*cb*sb*exp(k*xc(:,3)).*cos(theta);
        phixz=-k^2*cb*exp(k*xc(:,3)).*sin(theta);
        phiyy=-k^2*sb^2*exp(k*xc(:,3)).*cos(theta);
        phiyz=-k^2*sb*exp(k*xc(:,3)).*sin(theta);
        phizz=k^2*phi;        
        %
        % Take all of the derivatives of the potential using an order 2*alpha
        % FD scheme. This is implemented in the function ComputeMTermsFD.m.
        %
        alpha=2;
        
        [n,GradPhi,Grad2Phi,m]=ComputeMtermsFD(phi,xc,alpha,nu,nv,phin);
        
        ifig=ifig+1; figure(ifig); clf; hold on
        plot3(xc(:,1),xc(:,2),xc(:,3),'o')
        quiver3(xc(:,1), xc(:,2), xc(:,3), n(:,1), n(:,2),n(:,3), 'MarkerSize', 4); axis equal;
        AZ=40; EL=20; view(AZ,EL); xlabel('x'); ylabel('y'); zlabel('z'); axis('equal'); hold off;
       
        errx=(phix-GradPhi(:,1))/max(abs(phix));
        erry=(phiy-GradPhi(:,2))/max(abs(phiy));
        errz=(phiz-GradPhi(:,3))/max(abs(phiz));
        errxx=(phixx-Grad2Phi(:,1))/max(abs(phixx));
        errxy=(phixy-Grad2Phi(:,2))/max(abs(phixy));
        errxz=(phixz-Grad2Phi(:,3))/max(abs(phixz));
        erryy=(phiyy-Grad2Phi(:,4))/max(abs(phiyy));
        erryz=(phiyz-Grad2Phi(:,5))/max(abs(phiyz));
        errzz=(phizz-Grad2Phi(:,6))/max(abs(phizz));
        %
        %% Plot up all of the errors
        %
        
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),phi,'o'); xlabel('x'); ylabel('\phi'); grid on
        
        ifig=ifig+1; figure(ifig); clf;
        % plot(xc(:,1),phix,'o',xc(:,1),GradPhi(:,1),'+');
        plot(xc(:,1),errx,'o');
        xlabel('x'); ylabel('Error in u');
        grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        % plot(xc(:,1),phiy,'o',xc(:,1),GradPhi(:,2),'+');
        plot(xc(:,1),erry,'o');
        xlabel('x'); ylabel('Error in v');
        grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        % plot(xc(:,1),phiz,'o',xc(:,1),GradPhi(:,3),'+');
        plot(xc(:,1),errz,'o');
        xlabel('x'); ylabel('Error in w');
        grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),errxx,'o'); xlabel('x');
        ylabel('Error in u_x'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),erryy,'o'); xlabel('x');
        ylabel('Error in v_y'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),errzz,'o'); xlabel('x');
        ylabel('Error in w_z'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),errxy,'o'); xlabel('x');
        ylabel('Error in u_y'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),errxz,'o'); xlabel('x');
        ylabel('Error in u_z'); grid on;
        
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),erryz,'o'); xlabel('x');
        ylabel('Error in v_z'); grid on;
        %
        % Check the Laplacian which should be zero.
        %
        contFD=Grad2Phi(:,1)+Grad2Phi(:,4)+Grad2Phi(:,6);
        ifig=ifig+1; figure(ifig); clf;
        plot(xc(:,1),contFD,'o'); xlabel('x'); ylabel('div(u)'); grid on
        
end

