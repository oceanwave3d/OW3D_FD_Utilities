%
% This script tests 2D Taylor integration on a Wigley III hull. Since the 
% hull is defined by a 4th-order polynomial, the volume is integrated 
% essentially exacty for p>=4. (Note that there are some missing points 
% here where the error is 0). I'm not sure about the surface area since 
% the value I've taken is from numerical integration in Mathematica using 
% 15 digits of precision (the best I could get), which may or may not be 
% trustworthy for all digits. Based on this value though, the convergence
% seems to be p+1 or better. 
%
% Written by H. Bingham, DTU
%
% This version: Oct. 7, 2024. 
%
%%
addpath ../src
%%
clear all
ifig=0;
%
%
% Do a convergence test or not. 
%
% Convergence='no';
Convergence='yes'; 
%
% Method='Direct';      % Do the integral explicitly for each function.
Method='Indirect';    % Build the integration vector, which can be applied to any function. 
%
% Choose an even or cosine grid in the (u,v) space. The cosine grid 
% clusters the points towards both ends of both u and v. 
%
Grid='Even';
% Grid='Cos';
%
Wig=3;
L=1; B=L/10; T=L/16; 
%
%%
p=4; % The order of the schemes
alpha=p/2; r=2*alpha+1;
nv=5; nu=4*nv;
%
% Generate the hull points.
%
[x]=WigleyGrid(nu,nv,Wig,'structured',Grid);
%
% and the corresponding u-v space vectors.
%
switch Grid
    case 'Even'
        du=2/(nu-1); dv=2/(nv-1);
        u=-1+du*[0:nu-1]';
        v=-1+dv*[0:nv-1]';
    case 'Cos'
        th=-pi/2+pi*[0:nu-1]/(nu-1); u=sin(th);
        th=-pi/2+pi*[0:nv-1]/(nv-1); v=sin(th);
end
N=nu*nv; % The total number of grid points.
%
% Plot the geometry.
%
ifig=ifig+1; figure(ifig);clf; hold on
plot3(x(:,1),x(:,2),x(:,3),'o','MarkerSize',4); axis equal; grid on

switch Method
    case 'Direct'
        [D, nvec, ds]=TaylorIntegration2D_Direct_SetUp(x,u,v,p);
    case 'Indirect'
        [Ivec,nvec]=BuildTaylorIntegrationVector2D(x,u,v,p);
end
%
% Add the computed normal vectors. 
%
figure(ifig); 
quiver3(x(:,1), x(:,2), x(:,3), nvec(:,1), nvec(:,2),nvec(:,3));
%
% Set up the integration vector or coefficients and do the surface and
% volume integrals. 
%
switch Method
    case 'Direct'
        f=ds;
        S=2*TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,1).*x(:,1).*ds;
        V1=2*TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,2).*x(:,2).*ds;
        V2=2*TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,3).*x(:,3).*ds;
        V3=2*TaylorIntegrate2D_Direct(f,D);
    case 'Indirect'
        S=2*Ivec*ones(N,1);
        V1=-2*Ivec*(nvec(:,1).*x(:,1));
        V2=-2*Ivec*(nvec(:,2).*x(:,2));
        V3=-2*Ivec*(nvec(:,3).*x(:,3));
end
%
% Check the errors in the surface area and volume. 
%
Sex=0.15013441092291333;
display(['Relative error in the surface area: ',num2str((S-Sex)/Sex)])

Vex=13/4500;
Vol=(V1+V2)/2;
display(['Relative error in the volume: ',num2str((Vol-Vex)/Vex)])


%%
switch Convergence
    case 'yes'
        pvec=[2 4 6 8]; nvvec=[9,17,33,65]; nuvec=4*nvvec;
        L=1; B=L/10; T=L/16; 
        Sex=0.15013441092291333;
        Vex=13/4500;
        
        
        for io=1:length(pvec)
            p=pvec(io);
            alpha=p/2; r=2*alpha+1;
            for in=1:length(nuvec)
                nu=nuvec(in); nv=nvvec(in);
                [x]=WigleyGrid(nu,nv,Wig,'structured',Grid);
                switch Grid
                    case 'Even'
                        du=2/(nu-1); dv=2/(nv-1);
                        u=-1+du*[0:nu-1]';
                        v=-1+dv*[0:nv-1]';
                    case 'Cos'
                        th=-pi/2+pi*[0:nu-1]/(nu-1); u=sin(th);
                        th=-pi/2+pi*[0:nv-1]/(nv-1); v=sin(th);
                end
                N=nu*nv; % The total number of grid points.
                %
                [U,V]=meshgrid(u,v);
                xp=zeros(nv,nu); yp=xp; zp=xp;
                xp=L/2*U; zp=T/2*(1-V); 
                yp=B/2*(1-(zp/T).^2).*(1-(2*xp/L).^2).*(1+0.2*(2*xp/L).^2);
                zp=-zp;
                %
                x=zeros(N,3); x=[xp(:) yp(:) zp(:)]; 
                nvec=zeros(N,3); ds=zeros(N,1);
                %
                switch Method
                    case 'Direct'
                        [D, nvec, ds]=TaylorIntegration2D_Direct_SetUp(x,u,v,p);
                    case 'Indirect'
                        Ivec=zeros(N,1); 
                        [Ivec,nvec]=BuildTaylorIntegrationVector2D(x,u,v,p);
                end
                
                switch Method
                    case 'Direct'
                        f=ds;
                        S=2*TaylorIntegrate2D_Direct(f,D);
                        f=-nvec(:,1).*x(:,1).*ds;
                        V1=2*TaylorIntegrate2D_Direct(f,D);
                        f=-nvec(:,2).*x(:,2).*ds;
                        V2=2*TaylorIntegrate2D_Direct(f,D);
                    case 'Indirect'
                        S=2*Ivec*ones(N,1);
                        V1=-2*Ivec*(nvec(:,1).*x(:,1));
                        V2=-2*Ivec*(nvec(:,2).*x(:,2));
                        V3=-2*Ivec*(nvec(:,3).*x(:,3));
                end
                errS(in,io)=abs((S-Sex)/Sex);
                Vol=(V1+V2)/2;
                errV(in,io)=abs((Vol-Vex)/Vex);

            end
        end
        ifig=ifig+1; figure(ifig);clf
        loglog(nuvec,errS(:,1),'-+',nuvec,errS(:,2),'-o',nuvec,errS(:,3),'-*',...
            nuvec,errS(:,4),'-v')
        xlabel('n_u'); ylabel('Rel. Error in S');
        b2=polyfit(log(nuvec(end-2:end)),log(errS(end-2:end,1)'),1);
        b4=polyfit(log(nuvec(end-2:end)),log(errS(end-2:end,2)'),1);
        b6=polyfit(log(nuvec(end-2:end)),log(errS(end-2:end,3)'),1);
        b8=polyfit(log(nuvec(end-2:end)),log(errS(end-2:end,4)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
            ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
            'Location','SouthWest');
        grid on
        title([Grid,' spacing, n_v=n_u/4']);
        
        ifig=ifig+1; figure(ifig);clf
        loglog(nuvec,errV(:,1),'-+',nuvec,errV(:,2),'-o',nuvec,errV(:,3),'-*',...
            nuvec,errV(:,4),'-v')
        xlabel('n_u'); ylabel('Rel. Error in V');
        b2=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,1)'),1);
        b4=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,2)'),1);
        b6=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,3)'),1);
        b8=polyfit(log(nuvec(end-3:end-1)),log(errV(end-3:end-1,4)'),1);
        legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
            ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
            'Location','East');
        grid on
        title([Grid,' spacing, n_v=n_u/4']);

 end
