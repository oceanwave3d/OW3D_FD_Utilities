%
% This script tests two versions of 1D Taylor integration over a circular
% cylinder using uniform and non-uniform grid spacing. The first version: 
%
%          BuildTaylorIntegrationVector1D_ds_known()
%
% assumes that the arc-length along the curve is known exactly, while the
% second version: 
%
%          BuildTaylorIntegrationVector1D()
%
% only takes the grid point positions as input and computes the arc-lengths
% to the same order as requested for the Taylor expansion. 
%
% When the arc-length is given exactly, the integral of surface area and 
% volume over a circle are exact for any p>=2, where p+1 terms are taken 
% in the Taylor expansion. Convergence is order p+1 for general functions. 
% When arc length is not known exactly, convergence is order p. 
%
%%
% Written by H. B. Bingham, DTU, 2020.
% This version: Feb., 2024
%
%%
clear all
addpath ../src/

%%
% The integrals of the surface area and volume over a circle are exact for
% interpolation higher than 2, when the arc length and normal vector are 
% given exactly. 
%
% Cylinder radius and the expansion order to check. 
%
R=1; p=4;
%%
% Here u is the arc length, uniformly spaced over the circle. 
%
nu=10; 
Th=pi/2*[0:nu-1]'/(nu-1); 
u=R*Th; du=diff(u);

x(:,1)=R*cos(Th); x(:,2)=R*sin(Th);
nvec(:,1)=-cos(Th); nvec(:,2)=-sin(Th);

Ivec=BuildTaylorIntegrationVector1D_ds_known(u,p,du);

S=Ivec*ones(nu,1);
Sex=pi*R/2;
errS=abs((Sex-S)/Sex);

V1=-Ivec*(nvec(:,1).*x(:,1));
V2=-Ivec*(nvec(:,2).*x(:,2)); V=(V1+V2)/2;
Vex=pi*R^2/4;
errV = abs((Vex-V)/Vex);

display(['u=arc length, n exact, S error=',num2str(errS),', V error =',num2str(errV)])

%%
% Here u is the parametric unit-spaced coordinate and ds is given exactly.
%
u=[0:nu-1]'; du=ones(nu-1,1);

dth=pi/2/(nu-1); ds=R*dth;
Ivec=BuildTaylorIntegrationVector1D_ds_known(u,p,du);

S=ds*Ivec*ones(nu,1);
Sex=pi*R/2;
errS=abs((Sex-S)/Sex);

V1=-ds*Ivec*(nvec(:,1).*x(:,1));
V2=-ds*Ivec*(nvec(:,2).*x(:,2)); V=(V1+V2)/2;
Vex=pi*R^2/4;

errV = abs((Vex-V)/Vex);

display(['u=parametric unit-spaced, n exact, S error=',num2str(errS),', V error =',num2str(errV)])


%%
% Here, u is uniformly spaced from [0,1] and ds is given exactly, so ds is
% scaled by du. 
%
du=1/(nu-1);
u=du*[0:nu-1]'; duvec=du*ones(nu-1,1);

dth=pi/2/(nu-1); ds=R*dth;
Ivec=BuildTaylorIntegrationVector1D_ds_known(u,p,duvec);

S=ds/du*Ivec*ones(nu,1);
Sex=pi*R/2;
errS=abs((Sex-S)/Sex);


V1=-ds/du*Ivec*(nvec(:,1).*x(:,1));
V2=-ds/du*Ivec*(nvec(:,2).*x(:,2));
Vex=pi*R^2/4; V=(V1+V2)/2;

errV = abs((Vex-V)/Vex);

display(['u=parametric, n exact, S error=',num2str(errS),', V error =',num2str(errV)])

%%
% Here u is the unit-spaced parameter, and ds is computed from the surface
% gradients inside this version of the Taylor integration function. 
%
du=1;
u=du*[0:nu-1]'; th=pi/2*[0:nu-1]/(nu-1)';
x(:,1)=R*cos(th); x(:,2)=R*sin(th);

[Ivec,nvec]=BuildTaylorIntegrationVector1D(x,p);

S=Ivec*ones(nu,1);
Sex=pi*R/2;
errS=abs((Sex-S)/Sex);

V1=-Ivec*(nvec(:,1).*x(:,1)); 
V2=-Ivec*(nvec(:,2).*x(:,2)); 
Vex=pi*R^2/4; V=(V1+V2)/2;

errV = abs((Vex-V)/Vex);

display(['u=parametric, n from physical points, S error=',num2str(errS),', V error =',num2str(errV)])


%%
% Here u is non-uniformly space on [0,1], and ds is computed from the surface
% gradients inside this version of the Taylor integration function. 
%
dth=1/(nu-1); th=pi/2*[0:nu-1]'/(nu-1);
u=sin(th);
x(:,1)=R*cos(pi/2*u); x(:,2)=R*sin(pi/2*u);

[Ivec,nvec]=BuildTaylorIntegrationVector1D(x,p);

S=Ivec*ones(nu,1);
Sex=pi*R/2;
errS=abs((Sex-S)/Sex);

V1=-Ivec*(nvec(:,1).*x(:,1)); 
V2=-Ivec*(nvec(:,2).*x(:,2)); 
Vex=pi*R^2/4; V=(V1+V2)/2;

errV = abs((Vex-V)/Vex);

display(['u=non-uniform, n from physical points, S error=',num2str(errS),', V error =',num2str(errV)])

%%
% Convergence of the calculations with this approach using either even or 
% stretched grids. 
%
Grid='Even';
% Grid='Cos';
Method='General';  % Arc length computed
% Method='Ds';     % Arc length given exactly
%
pvec=[2 4 6 8]; nuvec=[9 16 32 64 100];
Sex=pi*R/2;
Vex=pi*R^2/4;
%
% The test function cos(kx)
%
kR=.5; ICoskx=pi/2*besselj(0,kR);


for io=1:length(pvec)
    p=pvec(io); alpha=p/2; r=p+1;
    for in=1:length(nuvec)
        nu=nuvec(in);
        nvec=zeros(nu,2);
        switch Grid
            case 'Cos'
                dth=1/(nu-1); th=pi/2*[0:nu-1]'/(nu-1);
                u=sin(th);
            case 'Even'
                du=1/(nu-1); u=[0:nu-1]'/(nu-1);
        end
        x=zeros(nu,2); 
        x(:,1)=R*cos(pi/2*u); x(:,2)=R*sin(pi/2*u);
        
        switch Method
            case 'General'
                [Ivec,nvec]=BuildTaylorIntegrationVector1D(x,p);
            otherwise
                nvec(:,1)=-cos(pi/2*u); nvec(:,2)=-sin(pi/2*u);
                u=pi/2*u; duvec=diff(u);
                Ivec=BuildTaylorIntegrationVector1D_ds_known(u,p,duvec);
                ds=ones(nu,1);
        end
        
        S=Ivec*ones(nu,1);
        errS(in,io)=abs((S-Sex)/Sex);
         
        V1=-Ivec*(nvec(:,1).*x(:,1));
        V2=-Ivec*(nvec(:,2).*x(:,2));
        V=(V1+V2)/2;
        errV(in,io)=abs((V-Vex)/Vex);
        
        f=cos(kR/R*x(:,1)); 
        If=Ivec*f;
        errf(in,io)=abs((If-ICoskx)/ICoskx);
    end
end
figure(1);clf
loglog(nuvec,errS(:,1),'-+',nuvec,errS(:,2),'-o',nuvec,errS(:,3),'-*',...
    nuvec,errS(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in S');
b2=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errS(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on
title([Grid,' spacing']);

figure(2);clf
loglog(nuvec,errV(:,1),'-+',nuvec,errV(:,2),'-o',nuvec,errV(:,3),'-*',...
    nuvec,errV(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in V');
b2=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errV(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on
title([Grid,' spacing']);

        
figure(3);clf
loglog(nuvec,errf(:,1),'-+',nuvec,errf(:,2),'-o',nuvec,errf(:,3),'-*',...
    nuvec,errf(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in f(x)=cos(kx)');
b2=polyfit(log(nuvec(end-3:end)),log(errf(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errf(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errf(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errf(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on
title([Grid,' spacing']);

        
