function [xp,nvec] = WigleyGrid(nx,nz,model,Grid,varargin)
%%
%
% Generate a unit length Wigley hull geometry on a structured or 
% unstrucutred mesh of nx by nz grid points. model=1,2,3, or 4 selects the
% Wigley model and Grid='structured', 'distmesh', 'random', or 'triangle'
% selects the grid type. For the unstructured grids, the total number of
% points may not be nx*nz, but should be close. The points are returned as
%
%  xp(1:N,3)
%
% along with the normal vectors nvec(1:N,1:3). 
%
% If 'distmesh' is chosen as the grid type, then this must be in the path.
%
% If 'triangle' is chosen as the grid type, nz should be odd. This is 
% checked and corrected if nz is even. 
%
% For the structured grid, the spacing can be chosen to be either uniform
% or cosine spaced towards the ends of each direction by including the
% optional argument, varargin='Cos'. 
%
% Written by H.B. Bingham, DTU, Oct. 2024.
%
%%
Grid2='Even';
switch Grid
    case 'structured'
        if (length(varargin) ~= 0)
            Grid2=varargin{1};
        end
end
%
% The unit length Wigley model.
%
L=1; T=L/16; 
switch model
    case 1
        alpha=1.e0; B=L/10.e0;
    case 2
        alpha=1.e0; B=L/5.e0;
    case 3
        alpha=0.e0; B=L/10.e0;
    case 4
        alpha=0.e0; B=L/5.e0;
end 
%
% Set up the u,v parameter space to generate the hull. 
%
dx=2/(nx-1); dz=1/(nz-1); 

switch Grid2
    case 'Even'
        U=-1+[0:nx-1]*dx; V=[0:nz-1]*dz;
    case 'Cos'
        th=-pi/2+pi*[0:nx-1]/(nx-1); U=sin(th);
        th=-pi/2+pi*[0:nz-1]/(nz-1); V=(1+sin(th))/2;
end

switch Grid
    case 'structured'
        [u,v]=meshgrid(U,V);
    case 'distmesh'
        dx=2/(nx-1); fac=2*nz/nx;
        fd=@(p) drectangle(p,-1,1,0,fac);
        [uv,t0]=distmesh2d(fd,@huniform,dx,[-1,0;1,fac],[-1,0;-1,fac;1,0;1,fac]);
        [uv,t0]=fixmesh(uv,t0);  % Ensure that the triangles are all ordered counter-clockwise
        % Make sure the boundary points are exactly on the boundary
        in=find(abs(uv(:,1)+1)<10^-7); uv(in,1)=-1;
        in=find(abs(uv(:,1)-1)<10^-7); uv(in,1)=1;
        in=find(abs(uv(:,2))<10^-7); uv(in,2)=0;
        in=find(abs(uv(:,2)-fac)<10^-7); uv(in,2)=fac;

        u=uv(:,1); v=uv(:,2)/fac; 
    case 'random'
        N=nx*nz; nb=2*(nx+nz)-4; n=N-nb;
        dx=2/(nx-1); dz=1/(nz-1); 
        U=-1+[0:nx-1]'*dx;  V=[0:nz-1]'*dz;
        % Fix the boundary points with a uniform spacing
        u=zeros(N,1); v=u;
        u(1:nz-1,1)=-1; v(1:nz-1,1)=V(1:nz-1,1);
        u(nz:nz+nx-2,1)=U(1:nx-1,1); v(nz:nz+nx-2,1)=1;
        u(nz+nx-1:nz+nx+nz-3,1)=1; v(nz+nx-1:nz+nx+nz-3,1)=V(nz:-1:2,1);
        u(2*nz+nx-2:nb,1)=U(nx:-1:2,1); v(2*nz+nx-2:nb,1)=0;
        % Then generate a random distribution of points inside
        u(nb+1:N,1)=-1+dx/2+(2-dx)*rand(n,1);
        v(nb+1:N,1)=dz/4+(1-dz/3)*rand(n,1); 
    case 'triangle'
        if mod(nz,2) ==0, nz=nz+1; end % Make sure that nz is odd. 
        [tmp1,tmp2]=meshgrid(U,V);
        ind=0;
        for j=1:2:nz-2
            j1=ind*(2*nx-1)+1;
            u(j1:j1+2*nx-2,1)=[tmp1(j,:),tmp1(j+1,1:nx-1)+dx/2]';
            v(j1:j1+2*nx-2,1)=[tmp2(j,:),tmp2(j+1,1:nx-1)]';
            ind=ind+1;
        end
        j1=ind*(2*nx-1)+1;
        u(j1:j1+nx-1,1)=tmp1(nz,:)';
        v(j1:j1+nx-1,1)=tmp2(nz,:)';
end
%
% The hull geometry
%
x=L*u/2; zp=1-v; z=-T*zp;
y=B/2*((1-zp.^2).*(1-u.^2).*(1+.2*u.^2)+alpha*zp.^2.*(1-zp.^8).*(1-u.^2).^4);

xp=[x(:),y(:),z(:)];
%
% The exact normal vectors 
%
HH=((1/4).*L.^2.*T.^2+(1/100).*B.^2.*(16.*T.^2.*u.^2.*((-2)+v).^2.* ...
    v.^2.*(2+u.^2+(-10).*alpha.*((-1)+u.^2).^3.*((-1)+v).^2.*(2+((-2)+ ...
    v)   .*v).*(2+((-2)+v).*v.*(2+((-2)+v).*v))).^2+L.^2.*((-1)+u.^2) ...
    .^2.*((-1)+v).^2.*(5+u.^2+(-5).*alpha.*((-1)+u.^2).^3.*(4+5.*((-2) ...
    +v).*v.*(2+((-2)+v).*v).*(2+((-2)+v).*v.*(2+((-2)+v).*v)))).^2)) ...
    .^(1/2);
tmp=(-2/5).*B.*T.*u.*((-2)+v).*v.*((-2)+(-1).*u.^2+10.*alpha.*((-1)+ ...
    u.^2).^3.*((-1)+v).^2.*(2+((-2)+v).*v).*(2+((-2)+v).*v.*(2+((-2)+ ...
    v).*v)));
nvec(:,1)=tmp(:)./HH(:);
nvec(:,2)=-L*T/2./HH(:);
tmp=(-1/10).*B.*L.*((-1)+u.^2).*((-1)+v).*((-5)+(-1).*u.^2+5.*alpha.*( ...
    (-1)+u.^2).^3.*(4+5.*((-2)+v).*v.*(2+((-2)+v).*v).*(2+((-2)+v).* ...
    v.*(2+((-2)+v).*v))));
nvec(:,3)=tmp(:)./HH(:);
     
        
