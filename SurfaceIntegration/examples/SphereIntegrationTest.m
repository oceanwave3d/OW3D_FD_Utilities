%
% This script tests 2D Taylor integration on a sphere. The parameter space
% mapping is singular in this case, but the results are
% convergent anyway. The normal vector and the element of surface area
% are not accurately computed at the singular point, but the scheme retains 
% roughly order p convergence even so. 
%
% Written by H. Bingham, DTU, June 15, 2022. Updated to correct the
% orientation of the u-v space Oct. 5, 2024. 
%
%%
addpath ../src
%%
clear all
ifig=0;
%
% Correct the normals and ds at the singular point? This doesn't seem to
% make much difference in fact, but the plot looks better with the correct
% normal vector at the pole.
%
% Correction='no';
Correction='yes';
%
% Choose the direct or the indirect method. In the direct method, all
% required derivatives are taken explicitly for the given function, then
% they are summed via Taylor integration to give the final result. In the
% indirect case, we build the integration vector for a given grid and
% expansion order p, then apply it to the function to get the integral. The
% indirect method is more elegant and more efficient. 
%
% Method='Direct';      
Method='Indirect';    
%

p=2; % The order of the schemes
alpha=p/2; r=2*alpha+1;

nu=10; nv=nu-1;
du=1/(nu-1); dv=1/(nv-1);
u=du*[0:nu-1]';
v=dv*[0:nv-1]';
N=nu*nv; % The total number of grid points.
%
% Define the geometry. Here a quarter of a hemisphere where the (u,v)
% origin is placed at the point z=-R and x=0 which is also the singular
% point of the mapping at the pole. U is the angle of the radius vector
% from the -z-axis and V follows the horizontal angle from the x-axis. 
%
R=1; 
[U,V]=meshgrid(u,v);
xp=zeros(nv,nu); yp=xp; zp=xp;
th=pi/2*U;
zp=-R*(cos(th)); xp=R*sin(th).*cos(pi/2*V); yp=R*sin(th).*sin(pi/2*V); 
%
% Plot the geometry.
%
ifig=ifig+1; figure(ifig);clf; hold on
plot3(xp,yp,zp,'o-')
%
% Put the coordinates into a vector.
%
x=[xp(:) yp(:) zp(:)];

switch Method
    case 'Direct'
        [D, nvec, ds]=TaylorIntegration2D_Direct_SetUp(x,u,v,p);
    case 'Indirect'
        [Ivec,nvec]=BuildTaylorIntegrationVector2D(x,u,v,p);
end

switch Correction
    case 'yes'
        ising=[1:nu-1];
        nvec(ising,1:2)=0; nvec(ising,3)=1; ds(ising,1)=0;
end

figure(ifig); hold on;
quiver3(x(:,1), x(:,2), x(:,3), nvec(:,1), nvec(:,2),nvec(:,3),...
    'MarkerSize', 5); axis equal;


switch Method
    case 'Direct'
        f=ds;
        S=TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,1).*x(:,1).*ds;
        V1=TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,2).*x(:,2).*ds;
        V2=TaylorIntegrate2D_Direct(f,D);
        f=-nvec(:,3).*x(:,3).*ds;
        V3=TaylorIntegrate2D_Direct(f,D);
    case 'Indirect'
        S=Ivec*ones(N,1);
        V1=-Ivec*(nvec(:,1).*x(:,1));
        V2=-Ivec*(nvec(:,2).*x(:,2));
        V3=-Ivec*(nvec(:,3).*x(:,3));
end
Sex=pi*R^2/2;
display(['Relative error in the surface area: ',num2str((S-Sex)/Sex)])

Vex=pi*R^3/6;
Vol=(V1+V2)/2;
display(['Relative error in the volume: ',num2str((Vol-Vex)/Vex)])

%%
% Integrate a synthetic pressure over the surface.
%
switch Method
    case 'Direct'
        f=x(:,1).*x(:,2).*ds; % p=xy
        Ifd=TaylorIntegrate2D_Direct(f,D);
    case 'Indirect'
        f=x(:,1).*x(:,2); % p=xy
        Ifd=Ivec*(f(:));
end
Iex=1/3;

display(['Relative error in f=xy, ',num2str((Ifd-Iex)/Iex)])

%%
% Do a convergence test for the surface area, the volume, and integration
% of the function xy. 
%
% Choose the expansion orders and the grid spacings to consider.
%
pvec=[2 4 6 8]; nuvec=[10 20 40 80]; 
R=1;
Sex=pi*R^2/2;
Vex=pi*R^3/6;
Ipex=1/3; % Exact integral of xy.
%
% Collect the errors for each combination of nu, nv, and p. 
%
for io=1:length(pvec)
    p=pvec(io);
    alpha=p/2; r=2*alpha+1;
    for in=1:length(nuvec)
        nu=nuvec(in); nv=nu;
        du=1/(nu-1); dv=1/(nv-1);
        u=du*[0:nu-1]'; v=dv*[0:nv-1]';
        N=nu*nv; % The total number of grid points.
        [U,V]=meshgrid(u,v);
        xp=zeros(nv,nu); yp=xp; zp=xp;
        th=pi/2*U;
        zp=-R*(cos(th)); xp=R*sin(th).*cos(pi/2*V); yp=R*sin(th).*sin(pi/2*V);
        %
        x=zeros(N,3); x=[xp(:) yp(:) zp(:)];
        nvec=zeros(N,3); ds=zeros(N,1);
        %
        switch Method
            case 'Direct'
                [D, nvec, ds]=TaylorIntegration2D_Direct_SetUp(x,u,v,p);
                switch Correction
                    case 'yes'
                        ising=[1:nu-1];
                        nvec(ising,1:2)=0; nvec(ising,3)=1; ds(ising,1)=0;
                end
                f=ds;
                S=TaylorIntegrate2D_Direct(f,D);
                f=-nvec(:,1).*x(:,1).*ds;
                V1=TaylorIntegrate2D_Direct(f,D);
                f=-nvec(:,2).*x(:,2).*ds;
                V2=TaylorIntegrate2D_Direct(f,D);
                f=x(:,1).*x(:,2).*ds;
                Ifd=TaylorIntegrate2D_Direct(f,D);
            case 'Indirect'
                Ivec=zeros(N,1);
                [Ivec,nvec]=BuildTaylorIntegrationVector2D(x,u,v,p);
                switch Correction
                    case 'yes'
                        ising=[1:nu-1];
                        nvec(ising,1:2)=0; nvec(ising,3)=1; ds(ising,1)=0;
                end
                S=Ivec*ones(N,1);
                V1=-Ivec*(nvec(:,1).*x(:,1));
                V2=-Ivec*(nvec(:,2).*x(:,2));
                V3=-Ivec*(nvec(:,3).*x(:,3));
                f=x(:,1).*x(:,2);
                Ifd=Ivec*f;
        end

        errS(in,io)=abs((S-Sex)/Sex);
        Vol=(V1+V2)/2;
        errV(in,io)=abs((Vol-Vex)/Vex);
        errP(in,io)=abs((Ifd-Ipex))/Ipex;
        
    end
end
%
% Plot the results
%
ifig=ifig+1; figure(ifig);clf
loglog(nuvec,errS(:,1),'-+',nuvec,errS(:,2),'-o',nuvec,errS(:,3),'-*',...
    nuvec,errS(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in S');
b2=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errS(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errS(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on

ifig=ifig+1; figure(ifig);clf
loglog(nuvec,errV(:,1),'-+',nuvec,errV(:,2),'-o',nuvec,errV(:,3),'-*',...
    nuvec,errV(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in V');
b2=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errV(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errV(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on

ifig=ifig+1; figure(ifig);clf
loglog(nuvec,errP(:,1),'-+',nuvec,errP(:,2),'-o',nuvec,errP(:,3),'-*',...
    nuvec,errP(:,4),'-v')
xlabel('n_p'); ylabel('Rel. Error in f=xy');
b2=polyfit(log(nuvec(end-3:end)),log(errP(end-3:end,1)'),1);
b4=polyfit(log(nuvec(end-3:end)),log(errP(end-3:end,2)'),1);
b6=polyfit(log(nuvec(end-3:end)),log(errP(end-3:end,3)'),1);
b8=polyfit(log(nuvec(end-3:end-1)),log(errP(end-3:end-1,4)'),1);
legend(['p=2, s=',num2str(b2(1))],['p=4, s=',num2str(b4(1))],...
    ['p=6, s=',num2str(b6(1))],['p=8, s=',num2str(b8(1))],...
    'Location','SouthWest');
grid on
