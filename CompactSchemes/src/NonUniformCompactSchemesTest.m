%%
%
% This script checks the convergence of compact 1st- to 4th-derivative
% schemes on a stretched grid of points. The boundary schemes are formed
% with reduced LHS stencil size and increased RHS stencils in order to
% preserve the banded structure of the LHS matrix and maintain the order of
% accuracy. This leads to a solvable system, while if the LHS stencil size
% is not reduced, the matrix turns out to be singular. Why, I'm not sure...
% 
% See the function 'FDInterpolateCompact.m' for a description of how the
% schemes are derived. 
%
% The schemes give the expected order of convergence, but the level of
% error at the boundary points is limited compared to the periodic case. 
%
% Written by H.B. Bingham, DTU, July 20, 2022.
%
%%
clear all
%
% Choose the left and right stencil sizes, the evaluation point, and the 
% derivative to approximate. 
%
rl=3;                % The LHS stencil size
rr=3;                % The RHS stencil for the centered scheme
der=1;               % The derivative to approximate
nt=rl+rr+der;        % The number of Taylor series terms to compute
all=(rl-1)/2;        % The number of neighbors to each side for the LHS scheme
al=(rr-1)/2;         % The total number of off-centered schemes to apply (we assume rr>=rl)
%
% Choose the grid type, either 'Even' or 'Tanh' for a stretched grid.
%
% Grid='Even';
Grid='Tanh'; Xstretch=1; 
%
% Choose the boundary conditions to apply: 'None', or 'NN' for two Neumann
% conditions.
%
BC='None';
% BC='NN';

%%
% In this general set up, we need to derive a new scheme for each point on
% the grid, so first loop over the number of grid points then loop over
% grid points and build the LHS and RHS matrices and check the error. 
%
nvec=[10,2.^[4:8]]'; Lx=1; k=2*pi; 

for in=1:length(nvec)
    n=nvec(in);
    switch Grid
        case 'Even'
            x=Lx*[0:n-1]'/(n-1);
        case 'Tanh'
            %
            % A stretched grid towards the left boundary.
            %
            ds=1/(n-1); s0=1; s=[0:n-1]'*ds;
            x=Lx*(1+tanh(Xstretch*(s-s0))/tanh(Xstretch));
    end
    %
    f=sin(k*x);
    switch der
        case 1
            fx=k*cos(k*x);
        case 2
            fx=-k^2*sin(k*x);
        case 3
            fx=-k^3*cos(k*x);
        case 4
            fx=k^4*sin(k*x);
    end
    %
    % Build the LHS and RHS matrices for this scheme and derivative.
    %
    [Al,Ar]=BuildCompactFD_Dx_1D(rl,rr,x,der);
    %
    % Solve for the derivative at all points.
    %
    switch BC
        case 'None'
            fxN=Al\(Ar*f);
        case 'NN'
            %
            % For two Neumann conditions, reduce the LHS by the two end
            % point equations and move the contributions to the RHS. 
            %
            Bp=sparse(2,n-2);Bp=Al(2:n-1,[1,n]);   % The end point constributions from the LHS
            Al([1,n],:)=[]; Al(:,[1,n])=[];        % Remove the rows and columns from Al
            Ar([1,n],:)=[];                        % Remove just the rows from Ar
            rhs=Ar*f(:,1) - Bp*[fx(1,1);fx(n,1)];  % Form the rhs
            fxN=Al\rhs; fxN=[fx(1,1);fxN;fx(n,1)]; % Solve and insert the boundary values
        case 'ND'
    end
    %
    % Evaluate the normalized error.
    %
    switch der
        case 1
            err(in,1)=max(abs(fxN-fx))/k;
            err(in,2)=mean(abs(fxN-fx))/k;
        case 2
            err(in,1)=max(abs(fxN-fx))/k^2;
            err(in,2)=mean(abs(fxN-fx))/k^2;
        case 3
            err(in,1)=max(abs(fxN-fx))/k^3;
            err(in,2)=mean(abs(fxN-fx))/k^3;
        case 4
            err(in,1)=max(abs(fxN-fx))/k^4;
            err(in,2)=mean(abs(fxN-fx))/k^4;
    end
end
%
% Plot the convergence.
%
figure(1); clf;
loglog(nvec,err(:,1),'-o')
switch rr
    case {2,3,4,5,6,7}
        fit=polyfit(log(nvec(2:4)),log(err(2:4,1)),1);
        text(10,10^-4,['slope=',num2str(fit(1))]);
    otherwise
        fit=polyfit(log(nvec(1:2)),log(err(1:2)),1);
        text(30,10^-10,['slope=',num2str(fit(1))]);
end
grid on; xlabel('n'); 
switch der
    case 1
        ylabel('Max. relative error in df/dx');
    case 2
        ylabel('Max. relative error in d^2f/dx^2');
    case 3
        ylabel('Max. relative error in d^3f/dx^3');
    case 4
        ylabel('Max. relative error in d^4f/dx^4');
end
title(['Convergence of a (',num2str(rl),',',num2str(rr),') compact scheme, f=sin(kx), ',Grid,' grid.'])

figure(2); clf;
loglog(nvec,err(:,2),'-o')
switch rr
    case {2,3,4,5,6,7}
        fit=polyfit(log(nvec(2:4)),log(err(2:4,2)),1);
        text(10,10^-6,['slope=',num2str(fit(1))]);
    otherwise
        fit=polyfit(log(nvec(1:2)),log(err(1:2,2)),1);
        text(30,10^-10,['slope=',num2str(fit(1))]);
end
grid on; xlabel('n'); 
switch der
    case 1
        ylabel('Mean relative error in df/dx');
    case 2
        ylabel('Mean relative error in d^2f/dx^2');
    case 3
        ylabel('Mean relative error in d^3f/dx^3');
    case 4
        ylabel('Mean relative error in d^4f/dx^4');
end
title(['Convergence of a (',num2str(rl),',',num2str(rr),') compact scheme, f=sin(kx), ',Grid,' grid.'])
