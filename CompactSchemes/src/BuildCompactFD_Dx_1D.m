function [Al,Ar]=BuildCompactFD_Dx_1D(rl,rr,x,der)
%
% Form the LHS and RHS matrices for the 1D grid x(1:n,1) using an rl-point
% stencil on the left and an rr-point stencil on the right. The strictly
% banded structure of the LHS matrix, Al, is kept by reducing the LHS
% stencil and increasing the RHS stencil for the boundary points to
% maintain a consistent order of accuracy for the overall scheme. 
%
% It is assumed that rr>=rl. 
%
% Written by H.B. Bingham, DTU, July 20, 2022.
%
%%
n=length(x);         % The number of grid points
all=(rl-1)/2;        % The number of neighbors to each side for the LHS scheme
al=(rr-1)/2;         % The total number of off-centered schemes to apply (we assume rr>=rl)
%
% Initialize the matrices.
%
Al=sparse(n,n); Ar=sparse(n,n);
%
%%
for i=1:al
    %
    % Preserve the banded structure of the LHS matrix by reducing the
    % LHS stencil and increasing the RHS stencil for the appropriate
    % boundary points.
    %
    if rl==rr || i==1
        shift=all+1-i;
        rli=rl-shift; il=[1:rli];
        rri=rr+shift+der-1; ir=[1:rri];
    else
        rli=rl;  il=[i-all:i+all];
        rri=rr;  ir=[1:rri];
    end
    %
    % Scale this stencil by the mean spacing and compute the scheme.
    %
    dx=mean(diff(x(1:rri)));
    xl=x(il)/dx; x0=x(i)/dx;
    xr=x(ir)/dx;
    
    [alpha,a]=FDInterpolateCompact(xl,xr,x0,der);
    %
    % Put these coefficients into the matrix
    %
    Al(i,il)=alpha; Ar(i,ir)=a./dx.^der;
end
for i=al+1:n-al
    %
    % Centered schemes
    %
    %
    % Scale this stencil by the mean spacing
    %
    dx=mean(diff(x(i-al:i+rr-al-1)));
    xl=x(i-all:i+all)/dx; x0=x(i)/dx;
    xr=x(i-al:i+al)/dx;
    
    [alpha,a]=FDInterpolateCompact(xl,xr,x0,der);
    %
    % Put these coefficients into the matrix
    %
    Al(i,i-all:i+all)=alpha; Ar(i,i-al:i+al)=a./dx^der;
end
for i=n-al+1:n
    %
    % Preserve the banded structure of the LHS matrix by reducing the
    % LHS stencil and increasing the RHS stencil for the boundary
    % points.
    %
    if rl==rr || i==n
        shift=all-(n-i);
        rli=rl-shift; il=[n-rli+1:n];
        rri=rr+shift+der-1; ir=[n-rri+1:n];
    else
        rli=rl;  il=[i-all:i+all];
        rri=rr;  ir=[n-rri+1:n];
    end
    %
    % Scale this stencil by the mean spacing and compute the scheme.
    %
    xr=x(ir);
    dx=mean(diff(xr));
    xr=xr/dx;
    xl=x(il)/dx; x0=x(i)/dx;
    
    [alpha,a]=FDInterpolateCompact(xl,xr,x0,der);
    %
    % Put these coefficients into the matrix vectors
    %
    Al(i,il)=alpha; Ar(i,ir)=a./dx.^der;
end
