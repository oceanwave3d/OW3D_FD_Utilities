%
% Check the convergence of solving \phi_{xx} = f using a compact schemes
% where the boundary conditions are imposed by adding a ghost point at each
% end. The ghost point equations are only included in the RHS of the
% Laplace operator, so for a grid of n physical points, the LHS is n by n
% and the RHS is n+2 by n+2. A Neumann condition is applied at the first
% point and a Dirichlet condition at the last point. 
%
% Written by H.B. Bingham, DTU, July 26, 2022.
%
%%
clear all

% The left and right stencil sizes for the compact FD operator. 
%
rl=3;
rr=3;
%
% Choose the grid type, either 'Even' or 'Tanh' for a stretched grid.
%
Grid='Even';
% Grid='Tanh'; Xstretch=1; 
%
% nvec=10;
nvec=[10,2.^[4:8]]'; 
Lx=1; k=2*pi; psi=pi/8; % The exact solution wavenumber and phase.
%
% Loop over the n-values and save the errors to check the convergence. 
%
for in=1:length(nvec)
    n=nvec(in);
    switch Grid
        case 'Even'
            x=Lx*[-1:n]'/(n-1);
        case 'Tanh'
            %
            % A stretched grid towards the left boundary.
            %
            ds=1/(n-1); s0=1; s=[0:n-1]'*ds;
            x=Lx*(1+tanh(Xstretch*(s-s0))/tanh(Xstretch));
            x=[-x(2,1);x;2-x(end-1,1)];
    end
    n=n+2;
    %
    f=sin(k*x+psi); fx=k*cos(k*x+psi); Lf=-k^2*sin(k*x+psi);
    %
    % Build the LHS and RHS matrices for the Laplacian
    %
    [Al,Ar]=BuildCompactFD_Dxx_1D_GP(rl,rr,x);
    %
    % Form the rhs of the Laplace problem
    %
    rhs=zeros(n,1);
    rhs(2:n-1,1)=Al*Lf(2:n-1,1); 
    rhs([1,n],1)=[fx(2,1);f(n-1,1)];  % Add the Neumann and Dirichlet BCs
    %
    % Solve for the function and compute the error. 
    %
    fN=Ar\rhs;
    err(in,1)=max(abs(fN(2:n-1,1)-f(2:n-1,1)));
    err(in,2)=mean(abs(fN(2:n-1,1)-f(2:n-1,1)));
end
%
% Plot the convergence.
%
figure(1); clf; 
loglog(nvec,err(:,1),'-o')
xlabel('n'); ylabel('Max. |f-f_e|'); 
sl=polyfit(log(nvec(2:4)),log(err(2:4,1)),1);
text(50,10^-5,['slope=',num2str(sl(1))]); grid on
title(['Convergence of a Laplace solve using the (',num2str(rl),',',num2str(rr),...
    ') compact scheme'])

figure(2); clf; 
loglog(nvec,err(:,2),'-o')
xlabel('n'); ylabel('Mean |f-f_e|'); 
sl=polyfit(log(nvec(2:4)),log(err(2:4,2)),1);
text(50,10^-5,['slope=',num2str(sl(1))]); grid on
title(['Convergence of a Laplace solve using the (',num2str(rl),',',num2str(rr),...
    ') compact scheme'])