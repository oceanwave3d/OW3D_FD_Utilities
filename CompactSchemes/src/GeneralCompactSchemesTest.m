%%
%
% This script checks the convergence of compact 1st- to 4th-derivative
% schemes on a uniform grid. See the function 'FDInterpolateCompact.m' for
% the details on how the schemes are derived. 
%
% The schemes are verified to give the expected order of
% convergence for 1st - 4th derivatives on a periodic grid. 
%
% Written by H.B. Bingham, DTU, July 17, 2022.
%
%%
clear all
%
% Choose the left and right stencil sizes, the evaluation point, and the 
% derivative to approximate. 
%
rl=3; xl=[0:rl-1];       % The LHS stencil 
i0=2; x0=i0-1; al=i0-1;  % The evaluation point and the number of points to the left
rr=3; xr=[0:rr-1];       % The RHS stencil
der=2;                   % The derivative to approximate
nt=rl+rr+der;            % The number of Taylor series terms to compute

%%
%
% Get the coefficients of the scheme. 
%
[alpha,a]=FDInterpolateCompact(xl,xr,x0,der);
%
% Display the coefficients
%
display(['alpha,beta,... ='])
rats(alpha)
display('a,b,c =')
rats(a)

%%
% Check the convergence of the scheme for a sine function on a periodic
% domain. 
%
nvec=[10,2.^[4:8]]'; k=2*pi; 

for i=1:length(nvec)
    n=nvec(i);
    dx=1/(n); x=[0:n-1]'*dx; f=sin(k*x); 
    switch der
        case 1
            fx=k*cos(k*x);
            fac=1/dx;
        case 2
            fx=-k^2*sin(k*x);
            fac=1/dx^2;
        case 3
            fx=-k^3*cos(k*x);
            fac=1/dx^3;
        case 4
            fx=k^4*sin(k*x);
            fac=1/dx^4;
    end
    %
    % Build the LHS matrix for the whole grid.
    %
    row=zeros(n,1); row(1:rl-al,1)=alpha(al+1:rl,1); row(n-al+1:n,1)=alpha(1:al,1);
    col=zeros(n,1); col(1:al+1,1)=alpha(al+1:-1:1,1); col(n-(rl-al)+2:n,1)=alpha(rl:-1:al+2,1); 
    Al=toeplitz(col,row');
    %
    % Build the RHS matrix for the whole grid and multiply with the
    % function to get the RHS vector. 
    %
    rhs=zeros(n,1);
    row=zeros(n,1); row(1:rr-al,1)=a(al+1:rr,1); row(n-al+1:n,1)=a(1:al,1);
    col=zeros(n,1); col(1:al+1,1)=a(al+1:-1:1,1); col(n-(rr-al)+2:n,1)=a(rr:-1:al+2,1);
    Ar=toeplitz(col,row');
    rhs=fac*Ar*f;
    %
    % Solve for the derivatives.
    %
    fxN=Al\rhs;
    %
    % Evaluate the normalized error.
    %
    switch der
        case 1
            err(i,1)=max(abs(fxN-fx))/k;
        case 2
            err(i,1)=max(abs(fxN-fx))/k^2;
        case 3
            err(i,1)=max(abs(fxN-fx))/k^3;
        case 4
            err(i,1)=max(abs(fxN-fx))/k^4;
    end

end
%
% Plot the convergence.
%
loglog(nvec,err,'-o')
switch rr
    case {2,3,4,5,6}
        fit=polyfit(log(nvec(1:3)),log(err(2:4)),1);
        text(10,10^-6,['slope=',num2str(fit(1))]);
    otherwise
        fit=polyfit(log(nvec(1:2)),log(err(1:2)),1);
        text(30,10^-10,['slope=',num2str(fit(1))]);
end
grid on; xlabel('n'); 
switch der
    case 1
        ylabel('Max. relative error in df/dx');
    case 2
        ylabel('Max. relative error in d^2f/dx^2');
    case 3
        ylabel('Max. relative error in d^3f/dx^3');
    case 4
        ylabel('Max. relative error in d^4f/dx^4');
end
title(['Convergence of the (',num2str(rl),',',num2str(rr),')-point compact scheme, f=sin(kx)'])
