function [Al,Ar]=BuildCompactFD_Dxx_1D_GP(rl,rr,x)
%%
%  A function to form the compact 1D Laplacian operator on the grid
%  x(1:n,1), where the first and last points are treated as ghost points to
%  impose a Neumann condition at x(2,1) and a Dirichlet condition at
%  x(n-1,1). The ghost point equations only appear in the RHS matrix Al
%  which is rank n, while the LHS matrix Al is rank n-2. I.e. the Laplacian
%  is assumed to be known at grid points 2 to n-1, but not at the ghost
%  points. 
%
% Written by H.B. Bingham, DTU, July 26, 2022. 
%
%%
% The size of the grid and the left and right stencils.
%
der=2;
n=length(x);         % The number of grid points, including ghost points
all=(rl-1)/2;        % The number of neighbors to each side for the LHS scheme
al=(rr-1)/2;         % The total number of off-centered schemes to apply (we assume rr>=rl)
%
% Initialize the matrices.
%
Al=sparse(n-2,n-2); Ar=sparse(n,n);
%
%%
% The first ghost point is a Neumann condition at grid point 2 imposed by
% an explicit scheme of the same order as the implicit Laplacian. 
%
i=1; 
rx=rl+rr-1;
c=FDInterpolate(x(2,1),x(1:rx,1),rx,1);
Ar(1,1:rx)=c(2,:);
%
% Off-centered schemes towards the left.
%
for i=1:al
    %
    % Preserve the banded structure of the LHS matrix by reducing the
    % LHS stencil and increasing the RHS stencil for the appropriate
    % boundary points.
    %
    if rl==rr || i==1
        shift=all+1-i;
        rli=rl-shift; il=[1:rli];
        rri=rr+shift; ir=[1:rri];
    else
        rli=rl;  il=[i-all:i+all];
        rri=rr;  ir=[1:rri];
    end
    %
    % Scale this stencil by the mean spacing and compute the scheme.
    %
    dx=mean(diff(x(1:rri)));
    xl=x(il+1)/dx; x0=x(i+1)/dx;
    xr=x(ir)/dx;
    
    [alpha,a]=FDInterpolateCompact(xl,xr,x0,der);
    %
    % Put these coefficients into the matrix
    %
    Al(i,il)=alpha; Ar(i+1,ir)=a./dx.^der;
end
%
% Centered schemes
%
for i=al+1:n-2-al
    %
    % Scale this stencil by the mean spacing
    %
    ir=i+1-al:i+rr-al; il=i+1-all:i+1+all;
    dx=mean(diff(x(ir)));
    xl=x(il)/dx; x0=x(i+1)/dx;
    xr=x(ir)/dx;
    
    [alpha,a]=FDInterpolateCompact(xl,xr,x0,der);
    %
    % Put these coefficients into the matrix
    %
    Al(i,il-1)=alpha; Ar(i+1,ir)=a./dx^der;
end
%
% Off-centered schemes towards the right.
%
for i=n-1-al:n-2
    %
    % Preserve the banded structure of the LHS matrix by reducing the
    % LHS stencil and increasing the RHS stencil for the boundary
    % points.
    %
    if rl==rr || i==n-2
        shift=all-(n-2-i);
        rli=rl-shift; il=[n-1-rli+1:n-1];
        rri=rr+shift; ir=[n-rri+1:n];
    else
        rli=rl;  il=[i+1-all:i+1+all];
        rri=rr;  ir=[n-rri+1:n];
    end
    %
    % Scale this stencil by the mean spacing and compute the scheme.
    %
    xr=x(ir);
    dx=mean(diff(xr));
    xr=xr/dx;
    xl=x(il)/dx; x0=x(i+1)/dx;
    
    [alpha,a]=FDInterpolateCompact(xl,xr,x0,der);
    %
    % Put these coefficients into the matrix vectors
    %
    Al(i,il-1)=alpha; Ar(i+1,ir)=a./dx.^der;
end
%
% A Dirichlet condition for the last ghost point.
%
i=n;
ir=n-rx+1:n; xr=x(ir,1); dx=mean(diff(xr));
c=FDInterpolate(x(n-1,1),xr,rx,dx);
Ar(n,ir)=c(1,:);

end
