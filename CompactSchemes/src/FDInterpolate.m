function [c mat m2]=FDInterpolate(x0,x,n,dx)
%
% A function to pass a polynomial interpolant through the n 
% points at positions x(1:n) and evaluate the function plus it's first 
% (n-1) derivatives at the position x0.  
%
% dx is a scale factor which should be a measure of the mean grid
% spacing for the problem at hand to help keep the matrix well 
% conditioned for high-order schemes.  
% 
% Written by H. B. Bingham, DTU
%
fact=1/dx;
for i=1:n
    for j=1:n
        mat(i,j)=(fact*(x(i)-x0))^(j-1)/factorial(j-1);
        m2(i,j)=mat(i,j);
    end
    m2(i,j+1)=(fact*(x(i)-x0))^(n)/factorial(n);
    m2(i,j+2)=(fact*(x(i)-x0))^(n+1)/factorial(n+1);
end 
c= inv(mat); 
% Re-scale the coefficients
for i=2:n, c(i,:)=fact^(i-1)*c(i,:); end 