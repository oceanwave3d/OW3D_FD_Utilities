function mat=TaylorMatrix(x,x0,nt)
%
% A function to build the Vandermonde matrix of Taylor series expansion 
% coefficients for a stencil of points x(1:n) about the expansion point x0.
% nt terms are kept at each point on the stencil. The coefficients are
% returned in the matrix: mat(1:nt,1:n). 
%
% Written by H. B. Bingham for course 41317 (CFD).
%
n=length(x);  % The size of the FD stencil
%
% Collect the Taylor expansion coefficients of the Vandermonde matrix.
for i=1:n
    for j=1:nt
        mat(j,i)=(x(i)-x0)^(j-1)/factorial(j-1);
    end
end 
