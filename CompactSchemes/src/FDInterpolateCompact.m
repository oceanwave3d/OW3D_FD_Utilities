function [alpha,a]=FDInterpolateCompact(xl,xr,x0,der)
%%
%   This function derives compact 1st- to 4th-derivative schemes using the
%   LHS stencil xl and the RHS stencil xr for the evaluation point x0. x0
%   must be one of the stencil points. der=1,2,3, or 4 indicates the
%   derivative to approximate. 
%
% The method applied follows the MSc thesis of Jonathan G. Tyler, Brigham 
% Young U. (2007). The schemes are of the form: 
% 
% alpha(1) f^{(m)}_1 + alpha(2) f^{(m)}_2 + alpha(3) f^{(m)}_3 + ... 
%
%      = a(1) f_1 + a(2) f_2 + a(3) f_3 + ...
%
% where f^{(m)}_i is the m-th derivative of the function at stencil 
% position i on the grid and f_i is the function value on this grid point. 
% At the evaluation point, i=i0, we set alpha(i0)=1, and solve for the
% other coefficients by setting (rl+rr-1) Taylor series expansion 
% coefficients to zero, where rl and rr are the number of coefficients on
% the left and right side of the equation respectively. 
%
% The script can be used to re-derive most of the schemes that appear in
% Tyler's Tables 1, 2, 3, 5, and 6, though I find slightly different values
% for a(1) in Table 6 for the last 4 schemes. 
%
% Written by H.B. Bingham, DTU, July 17, 2022.
%
%%
% Extract the stencil sizes and evaluation point. 
%
rl=length(xl); rr=length(xr); al=(max(rr,rl)-1)/2;
i0=find(xl==x0); 
nt=rl+rr+der;     % The number of Taylor series coefficients to keep
%
% Build the left and right Taylor series expansion matrices. Since the RHS
% scheme must also approximate the der^th-derivative, there are der extra
% equations which are included by adding der rows of zeros to the A matrix 
% and dropping the last der coefficients. This aligns the expansion
% coefficients in each row so that we can just add the two matrices. 
%
A =TaylorMatrix(xl,x0,nt); A=[zeros(der,rl);A(1:nt-der,:)];
B =TaylorMatrix(xr,x0,nt);
%
%%
% Collect the coefficients into a linear system of equations to solve for
% all of the unknown coefficients.
%
A(:,i0)=[];           % Remove the LHS column at the expansion point
Mat=[A,-B];
neq=size(Mat,2);      % The total number of unknowns
Mat=Mat(1:neq,:);     % Remove the extra Taylor series coefficients
%
% The rhs of the system has -1 in the row der+1 which sets the evaluation
% point alpha coefficient to 1.
%
rhs=zeros(neq,1); rhs(der+1,1)=-1;
% Solve
sol=Mat\rhs;
%
% Extract the solution
%
alpha(setdiff(1:rl,i0),1)=sol(1:rl-1,1); alpha(i0,1)=1;
a(:,1)=sol(rl:end,1);
end
