%
% Check the convergence of solving \phi_{xx} = f using a compact scheme.
% This works fine when both f and \phi_{xx}=0 at the two boundaries, but
% more general cases are more chanllenging.
%
%%
% The left and right stencil sizes
%
rl=3;
rr=3;
%
% Choose the grid type, either 'Even' or 'Tanh' for a stretched grid.
%
Grid='Even';
% Grid='Tanh'; Xstretch=1; 
%
% Boundary conditions to apply.
%
% BC='HD'; % Homogeneous Dirichlet at both ends (f_xx=0 as well).
BC='ND'; % Neumann at one end, Dirichlet at the other. 

% nvec=[10]'; 
nvec=[10,2.^[4:8]]'; 
Lx=1; k=2*pi; 

for in=1:length(nvec)
    n=nvec(in);
    switch Grid
        case 'Even'
            x=Lx*[0:n-1]'/(n-1);
        case 'Tanh'
            %
            % A stretched grid towards the left boundary.
            %
            ds=1/(n-1); s0=1; s=[0:n-1]'*ds;
            x=Lx*(1+tanh(Xstretch*(s-s0))/tanh(Xstretch));
    end
    %
    f=sin(k*x); fx=k*cos(k*x); Lf=-k^2*sin(k*x);
    %
    % Build the LHS and RHS matrices for the Laplacian
    %
    [Al,Ar]=BuildCompactFD_Dx_1D(rl,rr,x,2);
    switch BC
        case 'HD'
            Ar([1,n],:)=0; Ar(1,1)=1; Ar(n,n)=1;
            Al([1,n],:)=0; Al(1,1)=1; Al(n,n)=1;
            rhs=(Al*Lf);
        case 'ND'
            rx=rl+rr-1;
            c=FDInterpolate(x(1,1),x(1:rx,1),rx,1);
            Ar(1,:)=0; Ar(1,1:rx)=c(2,:); Al(1,:)=0; Al(:,1)=0; Al(1,1)=1; 
            Ar(n,:)=0; Ar(n,n)=1; Al(n,:)=0; Al(n,n)=1;
            rhs=(Al*Lf) + [fx(1,1);zeros(n-1,1)];
    end
    fN=Ar\rhs;
    err(in,1)=max(abs(fN-f));
end
figure(1); clf; 
loglog(nvec,err,'-o')
xlabel('n'); ylabel('Max. |f-f_e|'); 
sl=polyfit(log(nvec(2:4)),log(err(2:4)),1);
text(50,10^-5,['slope=',num2str(sl(1))]);
title(['Convergence of a Laplace solve using the (',num2str(rl),',',num2str(rr),...
    ') compact scheme'])