%%
%
% This script derives centered compact FD schemes on a uniform grid using a
% modified version of the method described in the MSc thesis of Jonathan G.
% Tyler, Brigham Young U. (2007). The schemes here only consider using the
% same stencil size on both the LHS and the RHS of the equation which is of
% the form: 
% 
% f^{(m)}_j + alpha(1)(f^{(m)}_{j-1}+f^{(m)}_{j+1}) + alpha(2) (f^{(m)}_{j-2}+f^{(m)}_{j+2} + ... 
%
%      = a(1) c_1(1,1:r)*f(1:r,1) + a(2) c_2(1,1:r)*f(1:r,1) + ...
%
% where f^{(m)} is the m-th derivative which we are trying to approximate, 
% and c_i, i=1,2,... are all of the possible centered FD schemes that can 
% be applied to approximate this derivative on an r-point stencil. 
%
% This form is more 'natural' to implement in Matlab than Tyler's form 
% which has a different collection of grid point differences on the RHS, 
% but it is easy to convert one form to the other and the results are 
% identical for all of the schemes that I have compared so far. The
% advantages of this form are that we can easily derive 'new' schemes that 
% do not appear in his thesis, and the code can be used to generate the
% coefficients and avoid typing them in manually. 
%
%
% Written by H.B. Bingham, DTU, July, 2022.
%
%%
clear all
%
% Choose the stencil size (must be odd) and the derivative. 
%
r=5;                                % The stencil size
der=1;                              % The derivative to approximate
nt=2*(r-1)+der;                     % The number of Taylor series terms to compute
al=(r-1)/2; xs=[-al:al]; x0=0;      % The number of neighbors to each side and the stencil 

%%
%
% Build the left and right Taylor series expansion matrices. For the LHS,
% the coefficients start with m=0, while on the RHS, the first der+1 terms
% will vanish when combined with the derivative schemes, and multiplying
% the LHS with dx^der shifts the RHS matrix up by der rows. 
%
B =TaylorMatrix(xs,x0,nt);
A=B(1:nt-der,:); 
B=B(der+1:nt,:);
%
%%
% Build the system matrix by adding the columns of A for each pair of 
% neighbors and multiplying B with each FD stencil. 
%
cx=zeros(r,al);
tmp=[];
%
% Accumulate the LHS expansions:
%
for i=1:al
    tmp=[tmp,A(:,i)+A(:,r-i+1)];
end
%
% Add in the RHS columns
%
switch der
    case {1,2}
        %
        % The product of B and each derivative scheme adds one column with
        % a minus sign. 
        %
        for i=1:al
            xs=[-i:i]; ri=2*i+1;
            c=FDInterpolate(x0,xs,ri,der);
            cx(al+1-i:al+1+i,i)=c(der+1,:)';
            tmp=[tmp, -B*cx(:,i)];
        end
    case {3,4}
        %
        % For 3rd- and 4th-derivatives, the minimum stencil size is 5.
        %
        for i=2:al
            xs=[-i:i]; ri=2*i+1;
            c=FDInterpolate(x0,xs,ri,der);
            cx(al+1-i:al+1+i,i-1)=c(der+1,:)';
            tmp=[tmp, -B*cx(:,i-1)];
        end
end
neq=size(tmp,2);
%
% Due to the centered schemes, every other row is identically zero, so
% extract the non-zero rows to produce a square system. 
%
Mat=tmp(1:2:2*neq,:);
%
% The rhs of the system has -1 in the first row which sets the central
% alpha coefficient to 1. 
%
rhs=zeros(neq,1); rhs(1,1)=-1;
sol=Mat\rhs;
%
% Extract the solution
%
alpha=[sol(1:al,1);1;sol(al:-1:1)];
a(:,1)=sol(al+1:end,1);
%
% Compare the result to that of Tyler Tables 1 & 2.
%
display(['alpha,beta,... ='])
rats(alpha)
switch der
    case 1
        display('a,b,c in Tyler notation:')
        switch r
            case 3
                rats(2*cx(3,1)*a(1))
            case 5
                rats(2*(cx(4,1)*a(1)+cx(4,2)*a(2)))
                rats(4*cx(5,2)*a(2))
            case 7
                rats(2*(cx(5,1)*a(1)+cx(5,2)*a(2)+2*cx(5,3)*a(3)))
                rats(4*(cx(6,2)*a(2)+cx(6,3)*a(3)))
                rats(6*cx(7,3)*a(3))
            otherwise
                rats(a)
        end
    otherwise
        display('a,b,c =')
        rats(a)
end

%%
% Check the convergence of the scheme for a sine function on a periodic
% domain. 
%
nvec=2.^[3:8]'; k=2*pi; 

for i=1:length(nvec)
    n=nvec(i);
    dx=1/(n); x=[0:n-1]'*dx; f=sin(k*x); 
    switch der
        case 1
            fx=k*cos(k*x);
            fac=1/dx;
        case 2
            fx=-k^2*sin(k*x);
            fac=1/dx^2;
        case 3
            fx=-k^3*cos(k*x);
            fac=1/dx^3;
        case 4
            fx=k^4*sin(k*x);
            fac=1/dx^4;
    end
    %
    % Build the LHS matrix for the whole grid.
    %
    row=zeros(n,1); row(1:al+1,1)=alpha(al+1:r,1); row(n-al+1:n,1)=alpha(1:al,1); 
    col=zeros(n,1); col(1:al+1,1)=alpha(al+1:-1:1,1); col(n-al+1:n,1)=alpha(r:-1:al+2,1); 
    Al=toeplitz(col,row');
    %
    % Build the RHS matrices for the whole grid and accumulate the products
    % with the function. 
    %
    rhs=zeros(n,1);
    for j=1:length(a)
        row=zeros(n,1); row(1:al+1,1)=cx(al+1:r,j); row(n-al+1:n,1)=cx(1:al,j);
        col=zeros(n,1); col(1:al+1,1)=cx(al+1:-1:1,j); col(n-al+1:n,1)=cx(r:-1:al+2,j);
        Ar=toeplitz(col,row');
        rhs=rhs+a(j)*fac*Ar*f;
    end
    %
    % Solve for the derivative.
    %
    fxN=Al\rhs;
    %
    % The relative error.
    %
    switch der
        case 1
            err(i,1)=max(abs(fxN-fx))/k;
        case 2
            err(i,1)=max(abs(fxN-fx))/k^2;
        case 3
            err(i,1)=max(abs(fxN-fx))/k^3;
        case 4
            err(i,1)=max(abs(fxN-fx))/k^4;
    end

end
%
% Plot the convergence.
%
loglog(nvec,err,'-o')
switch r
    case {3,5}
        fit=polyfit(log(nvec(1:3)),log(err(2:4)),1);
        text(10,10^-6,['slope=',num2str(fit(1))]);
    otherwise
        fit=polyfit(log(nvec(1:2)),log(err(1:2)),1);
        text(30,10^-10,['slope=',num2str(fit(1))]);
end
grid on; xlabel('n'); 
switch der
    case 1
        ylabel('Max. relative error in df/dx');
    case 2
        ylabel('Max. relative error in d^2f/dx^2');
    case 3
        ylabel('Max. relative error in d^3f/dx^3');
    case 4
        ylabel('Max. relative error in d^4f/dx^4');
end
title(['Convergence of the ',num2str(r),'-point compact scheme, f=sin(kx)'])
